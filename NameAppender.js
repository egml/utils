import Name from './Name';

var glue = {
	'camel': null,
	'capitalCamel': null,
	'kebab': '-',
	'snake': '_',
	'flat': null,
	'dot': '.',
};

export default function NameAppender(base, type) {
	this.self = base;
	this.type = type;
	
	this.append = function(bits)
	{
		return new NameAppender(new Name(bits, this.base), this.type);
	};
}

StringCase.prototype.toString = function() {
	return this.base[this.type];
};