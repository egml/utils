import { stringCase } from '@egml/utils';
import StringCase from './StringCase';

export default function Name(nameBits, nsBits) {
	var bits;

	// Далее идет обработка параметра nsBits:
	// - если он не задан, пропустить все и взять ns из прототипа
	// - если он boolean и true, то также пропустить и использовать ns 
	//   из прототипа
	// - если он boolean и false, то nsBits - пустая строка
	// - если nsBits строка или массив, то создать новое свойство 'ns' 
	//   поверх геттера прототипа со значением nsBits
	// - если nsBits представляет из себя другой Name, то создать новое 
	//   свойство 'ns' поверх геттера прототипа со значением nsBits
	if (nsBits != null) {
		if (typeof nsBits == 'boolean' && nsBits == false) {
			nsBits = '';
		}
		if (typeof nsBits == 'string' || Array.isArray(nsBits)) {
			Object.defineProperty(this, 'ns', {
				value: new StringCase(nsBits),
				writable: true,
				configurable: true,
				enumerable: true,
			});
		}
		if (nsBits instanceof Name) {
			Object.defineProperty(this, 'ns', {
				value: nsBits,
				writable: true,
				configurable: true,
				enumerable: true,
			});
		}
	}

	var hasNs = this.ns && this.ns.toString() != '';

	this.base = new StringCase(nameBits);

	// #asIs #flat #camel #capital #kebab
	var types = ['asIs', 'flat', 'camel', 'capitalCamel', 'kebab'];
	for (var i = 0; i < types.length; i++) {
		if (hasNs) {
			if (this.ns instanceof Name) {
				this['_' + types[i]] = stringCase([this.ns['_' + types[i]], this.base[types[i]]], types[i]);
			} else {
				this['_' + types[i]] = stringCase([this.ns[types[i]], this.base[types[i]]], types[i]);
			}
		} else {
			this['_' + types[i]] = this.base[types[i]];
		}
		this[types[i]] = (function(type, nameObj) {
			return function(suffix) {
				if (suffix) {
					if (type != 'asIs') {
						if (Array.isArray(suffix)) {
							suffix.forEach(function(bit, i) {
								suffix[i] = bit.toLowerCase();
							});
						}
					}
					return stringCase([nameObj['_' + type], stringCase(suffix, type)], type);
				} else {
					return nameObj['_' + type];
				}
			};
		})(types[i], this);
	}

	//  ██████   ██████  ████████
	//  ██   ██ ██    ██    ██
	//  ██   ██ ██    ██    ██
	//  ██   ██ ██    ██    ██
	//  ██████   ██████     ██
	//
	// #dot
	var bits = [ this.base.camel ];
	if (hasNs) {
		if (this.ns instanceof Name) {
			bits.unshift(this.ns._dot);
		} else {
			bits.unshift(this.ns.camel);
		}
	}
	this._dot = stringCase(bits, 'dot');
	this.dot = function(suffix) {
		if (suffix) {
			if (Array.isArray(suffix)) {
				suffix.forEach(function(bit, i) {
					suffix[i] = bit.toLowerCase();
				});
			}
			return stringCase([this._dot, stringCase(suffix, 'camel')], 'dot');
		} else {
			return this._dot;
		}
	};

	//  ███████ ██    ██ ███████ ███    ██ ████████
	//  ██      ██    ██ ██      ████   ██    ██
	//  █████   ██    ██ █████   ██ ██  ██    ██
	//  ██       ██  ██  ██      ██  ██ ██    ██
	//  ███████   ████   ███████ ██   ████    ██
	//
	// #event
	this.event = this.dot;

	//   ██████ ███████ ███████
	//  ██      ██      ██
	//  ██      ███████ ███████
	//  ██           ██      ██
	//   ██████ ███████ ███████
	//
	// #css
	bits = [ this.base.snake ];
	if (hasNs) {
		if (this.ns instanceof Name) {
			bits.unshift(this.ns._css);
		} else {
			bits.unshift(this.ns.snake);
		}
	}
	this.css = function(suffix) {
		if (suffix) {
			if (Array.isArray(suffix)) {
				suffix.forEach(function(bit, i) {
					suffix[i] = bit.toLowerCase();
				});
			}
			return stringCase([this._css, stringCase(suffix, 'snake')], 'kebab');
		} else {
			return this._css;
		}
	};

	//   ██████ ███████ ███████
	//  ██      ██      ██
	//  ██      ███████ ███████
	//  ██           ██      ██
	//   ██████ ███████ ███████
	// 
	//  ███    ███  ██████  ██████
	//  ████  ████ ██    ██ ██   ██
	//  ██ ████ ██ ██    ██ ██   ██
	//  ██  ██  ██ ██    ██ ██   ██
	//  ██      ██  ██████  ██████
	//
	// #css #modificator
	this.cssModificator = function(modificatorName) {
		if (modificatorName != null) {
			if (Array.isArray(modificatorName)) {
				modificatorName.forEach(function(bit, i) {
					modificatorName[i] = bit.toLowerCase();
				});
			}
			return this.css() + '--' + stringCase(modificatorName, 'snake');
		} else {
			return this.css();
		}
	};
	this.cssMod = this.cssModificator;

	//   ██████ ███████ ███████
	//  ██      ██      ██
	//  ██      ███████ ███████
	//  ██           ██      ██
	//   ██████ ███████ ███████
	// 
	//   ██████ ██       █████  ███████ ███████
	//  ██      ██      ██   ██ ██      ██
	//  ██      ██      ███████ ███████ ███████
	//  ██      ██      ██   ██      ██      ██
	//   ██████ ███████ ██   ██ ███████ ███████
	// 
	//  ███████ ███████ ██      ███████  ██████ ████████  ██████  ██████
	//  ██      ██      ██      ██      ██         ██    ██    ██ ██   ██
	//  ███████ █████   ██      █████   ██         ██    ██    ██ ██████
	//       ██ ██      ██      ██      ██         ██    ██    ██ ██   ██
	//  ███████ ███████ ███████ ███████  ██████    ██     ██████  ██   ██
	//
	// #css #class #selector
	this.cssClassSelector = function(appendage) {
		return '.' + this.css(appendage);
	};
	this.selector = function(appendage) {
		return this.cssClassSelector(appendage);
	};
}

var ns;

Object.defineProperty(Name.prototype, 'ns', {
	set: function(value) {
		if (value instanceof Name) {
			ns = value;
		} else {
			ns = new StringCase(value);
		}
	},
	get: function() {
		return ns;
	},
});

Name.prototype.toString = function() {
	return this._asIs;
};