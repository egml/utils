## Utils

Набор полезных наработок на **JavaScript** и **Sass**.

### JavaScript

Утилиты написаны в стиле ES6. Некоторые из них используются также в Node.js, поэтому они транспилируются в совсместимый формат в директорию `node`.

Команда для транспилирования таких утилит:

	$ yarn transpile

### Sass

Различные наработки, в том числе стартовый набор для верстки сайта. Все переменные определены с `!default`.

Если установлен модуль `node-sass-import`:

```scss
// ... настройка переменных тут ...

// Подключение только базовых переменных, функций и миксинов
@import '@egml/utils/utils';

// ... или тут ...

// Набор: все компоненты без вывода правил
//  - normalize
//  - nullify
//  - text
//  - edge
//  - gap
//  - row
//  - paragraph
//  - placeholder
//  - contain-margins
@import '@egml/utils/preset-basic';

// Набор: основа для верстки сайта
//  - preset-basic (набор)
//  - website-base
//  - text.rules
//  - row.rules
//  - paragraph.rules
//  - spacer
@import '@egml/utils/preset-website';

// ... но не тут ...
```

Пример использования:

```scss	
@import '@egml/utils/utils';
$utils-namespace: 'project';
$utils-media: 'mobile' 'desktop' 'tv';
$utils-media-breakpoint: (
	'mobile-desktop': 640,
	'desktop-tv': 1300,
);
@import '@egml/utils.scss/preset-website';
```

### Сборка

Для сборки выполнить в директории репозитория:

	$ yarn
	$ yarn grunt build
	$ yarn webpack

TODO:

- `#TODO` 

Сделано:

- `#DONE` 