const path = require('path');
const webpack = require('webpack');
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
	mode: 'development',
	entry: {
		main: './demo/main',
	},
	output: {
		path: path.resolve(__dirname, 'demo/'),
		filename: '[name]-build.js',
	},
	plugins: [
		new WebpackNotifierPlugin(),
	],
	devtool: 'inline-source-map',
};
