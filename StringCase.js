import { stringCase } from './utils';

export default function StringCase(bits) {
	this.bits = bits;

	if (Array.isArray(bits)) {
		this.asIs = this.bits.join('');
		bits.forEach(function(bit, i) {
			bits[i] = bit.toLowerCase();
		});
	} else {
		this.asIs = this.bits;
		bits = bits.toLowerCase();
	}

	this.camel = stringCase(bits, 'camel');
	this.capitalCamel = stringCase(bits, 'capitalCamel');
	this.kebab = stringCase(bits, 'kebab');
	this.snake = stringCase(bits, 'snake');
	this.flat = stringCase(bits, 'flat');
	this.dot = stringCase(bits, 'dot');
}

StringCase.prototype.toString = function() {
	return this.asIs;
};