const sass = require('node-sass');
// const sass = require('sass'); // Выдает больше информации при ошибке, но работает в 4 и более раз медленнее
// const sassImporter = require('node-sass-import');

var Name = require('./node/Name').default;
var pkg = require('./package.json');
var name = new Name(pkg[pkg.name].name, pkg[pkg.name].namespace);

module.exports = grunt => {
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		pkg: pkg,
		name: name,
		
		//	███████  █████  ███████ ███████
		//	██      ██   ██ ██      ██
		//	███████ ███████ ███████ ███████
		//	     ██ ██   ██      ██      ██
		//	███████ ██   ██ ███████ ███████
		// 
		//	#sass

		sass: {
			options: {
				implementation: sass,
				sourceMap: false,
				outputStyle: 'expanded',
				indentType: 'tab',
				indentWidth: 1,
				// importer: sassImporter,
			},
			build: {
				// options: {
				// 	sourceMap: true,
				// 	outputStyle: 'compressed',
				// },
				files: [
					{
						cwd: 'test',
						src: [ '**/*.scss' ],
						dest: 'test',
						ext: '.css',
						expand: true,
					}
				],
			},
		},

		//	██     ██  █████  ████████  ██████ ██   ██
		//	██     ██ ██   ██    ██    ██      ██   ██
		//	██  █  ██ ███████    ██    ██      ███████
		//	██ ███ ██ ██   ██    ██    ██      ██   ██
		//	 ███ ███  ██   ██    ██     ██████ ██   ██
		//
		//	#watch

		watch: {
			options: {
				spawn: false,
			},
			css: {
				files: [
					'test/**/*.scss',
				],
				tasks: [
					'sass',
				],
			},
		},
	});
};