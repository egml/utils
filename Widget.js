import { isObject, extend, firstUpperCase } from './utils';
import Name from './Name';
import Id from './Id';

/**
 * Базовый класс для виджетов
 * @param {Array-like} arguments ...
 */
export default function Widget() {
	this.construct(...arguments);
};

/**
 * Переопределение свойства `name` конструктора нужно при сохранении экзепляра класса в прототипе
 * (см. метод `saveInstanceOf`)
 */
Object.defineProperty(Widget, 'name', {
	value: new Name('Widget', false),
});

Widget.prototype.name = new Name('widget');
Widget.prototype.debug = false;
Widget.prototype.busy = false;
Widget.prototype.initialized = false;
Widget.prototype.autoInitialize = true;
Widget.prototype.mountPointFontSize = 16;

/**
 * Конструирование объекта.
 * Этот метод не задуман для переопределения или расширения (для этого см. `constructionChain`).
 * @param {Array-like} arguments ...
 */
Widget.prototype.construct = function()
{
	var target, options;
	if (arguments[0] instanceof Element) {
		target = arguments[0];
		if (!!arguments[1] && typeof arguments[1] == 'object') {
			options = arguments[1];
			if (typeof arguments[2] == 'boolean') {
				options.autoInitialize = arguments[2];
			}
		}
	} else if (!!arguments[0] && typeof arguments[0] == 'object') {
		options = arguments[0];
		if (typeof arguments[1] == 'boolean') {
			options.autoInitialize = arguments[1];
		}
	}
	this.constructionChain(target, options);
	if (this.autoInitialize) {
		this.initialize(this.dom.self);
	}
};

/**
 * Конструирование объекта (только относящееся к этом объекту + вызов цепочки). Тут только тот код, 
 * который нельзя назвать инициализацией (который не должен исполняться повторно). Этот метод задуман 
 * для расширения или переопределения дочерними классами.
 * @param {HTMLElement} target Элемент DOM, на котором создается виджет (иногда он нужен в конструкторе)
 * @param {Object} options Параметры для экземпляра класса (перезаписывает значения по умолчанию)
 */
Widget.prototype.constructionChain = function(target, options)
{
	this.dom = {
		self: target,
	};
	this.id = new Id;
	this.setOptions(options);
	this.saveInstanceOf(Widget);
};

/**
 * Инициализация экзепляра класса: привязка к DOM, назначение прослушки событий и т.п.
 * Этот метод не задуман для переопределения или расширения (для этого см. `initializationChain`)
 * @param {HTMLElement|null} target Элемент DOM, который будет основным при работе виджета
 */
Widget.prototype.initialize = function(target)
{
	if (typeof target == 'undefined') {
		target = this.dom.self; // При отложенной инициализации
	}
	this.initializationChain(target);
	this.initialized = true;
};

/**
 * Это псевдоним для `initialize`.
 * @param {HTMLElement|null} target Элемент DOM, который будет основным при работе виджета
 */
Widget.prototype.mount = function(target)
{
	this.initialize(target);
};

/**
 * Инициализация экзепляра класса (только относящаяся к этому объекту + вызов цепочки): 
 * привязка к DOM, назначение прослушки событий и т.п. Этот метод задуман для расширения 
 * или переопределения дочерними классами.
 * @param {HTMLElement} target Элемент DOM, на котором создается виджет
 */
Widget.prototype.initializationChain = function(target)
{
	this.dom.self = target;
};

Widget.prototype.setOptions = function(options)
{
	if (isObject(options)) {
		extend(this, options);
	}
};

// /*
//  ███████  █████  ██    ██ ███████
//  ██      ██   ██ ██    ██ ██
//  ███████ ███████ ██    ██ █████
//       ██ ██   ██  ██  ██  ██
//  ███████ ██   ██   ████   ███████
// 
//  ██ ███    ██ ███████ ████████  █████  ███    ██  ██████ ███████
//  ██ ████   ██ ██         ██    ██   ██ ████   ██ ██      ██
//  ██ ██ ██  ██ ███████    ██    ███████ ██ ██  ██ ██      █████
//  ██ ██  ██ ██      ██    ██    ██   ██ ██  ██ ██ ██      ██
//  ██ ██   ████ ███████    ██    ██   ██ ██   ████  ██████ ███████
// 
//   ██████  ███████
//  ██    ██ ██
//  ██    ██ █████
//  ██    ██ ██
//   ██████  ██
// */
// #save #instance #of
/**
 * Сохранение экземпляра класса в регистре экземпляров класса этого типа
 * @param {Function} constructor Конструктор класса, в который происходит сохранение экземпляра
 */
Widget.prototype.saveInstanceOf = function(constructor)
{
	constructor = constructor || this.constructor;
	if (!constructor.prototype.hasOwnProperty('instances')) {
		(constructor.prototype.instances = []).length++; // потому что мы хотим считать id с 1, а не с 0
		// #TODO: Переделать на что-то более корректное, ведь при таком подходе с массивом его length будет выдавать результат не единицу больший, чем в действительности есть сохраненных экзепляров. Возможно лучше использовать Set или Map.
	}
	this.id.current = constructor.prototype.instances.length;
	this.id[constructor.name._camel] = this.id >> 0; // скастовать в строку, а затем в целое число
	constructor.prototype.instances[this.id] = this;
};

// /*
//  ███████ ███████ ████████
//  ██      ██         ██
//  ███████ █████      ██
//       ██ ██         ██
//  ███████ ███████    ██
// 
//  ██████   █████  ████████  █████  ███████ ███████ ████████
//  ██   ██ ██   ██    ██    ██   ██ ██      ██         ██
//  ██   ██ ███████    ██    ███████ ███████ █████      ██
//  ██   ██ ██   ██    ██    ██   ██      ██ ██         ██
//  ██████  ██   ██    ██    ██   ██ ███████ ███████    ██
// 
//  ██ ██████   ██████  ███████
//  ██ ██   ██ ██    ██ ██
//  ██ ██   ██ ██    ██ █████
//  ██ ██   ██ ██    ██ ██
//  ██ ██████   ██████  ██
// */
// #set #dataset #id #of
Widget.prototype.setDatasetIdOf = function(constructor)
{
	constructor = constructor || this.constructor;
	var id = this.id[constructor.name._camel];
	var name = constructor.prototype.name.camel('id');
	this.dom.self.dataset[name] = id;
};

// /*
//  ██ ███    ██ ███████ ████████  █████  ███    ██  ██████ ███████
//  ██ ████   ██ ██         ██    ██   ██ ████   ██ ██      ██
//  ██ ██ ██  ██ ███████    ██    ███████ ██ ██  ██ ██      █████
//  ██ ██  ██ ██      ██    ██    ██   ██ ██  ██ ██ ██      ██
//  ██ ██   ████ ███████    ██    ██   ██ ██   ████  ██████ ███████
// 
//  ███████ ██████   ██████  ███    ███
//  ██      ██   ██ ██    ██ ████  ████
//  █████   ██████  ██    ██ ██ ████ ██
//  ██      ██   ██ ██    ██ ██  ██  ██
//  ██      ██   ██  ██████  ██      ██
// 
//  ██████   █████  ████████  █████  ███████ ███████ ████████
//  ██   ██ ██   ██    ██    ██   ██ ██      ██         ██
//  ██   ██ ███████    ██    ███████ ███████ █████      ██
//  ██   ██ ██   ██    ██    ██   ██      ██ ██         ██
//  ██████  ██   ██    ██    ██   ██ ███████ ███████    ██
// */
// #instance #from #dataset
Widget.prototype.instanceFromDataset = function(element, constructor)
{
	return constructor.prototype.instances[element.dataset[constructor.prototype.name.camel('id')]];
};

Widget.prototype.setState = function(state)
{
	var methodName = 'setState' + firstUpperCase(state);
	if (typeof this[methodName] == 'function') {
		this[methodName]();
	}
};

Widget.prototype.clearState = function(state)
{
	if (state == 'all') {
		// обход всех
		for (var property in this) {
			if (property.match(/^clearState(?!All).+/)) {
				this[property]();
			}
		}
	} else {
		if (!state) {
			if (!this.state) return;
			state = this.state;
		}
		var methodName = 'clearState' + firstUpperCase(state);
		if (typeof this[methodName] == 'function') {
			this[methodName]();
		}
	}
};