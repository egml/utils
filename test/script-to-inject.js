console.log('===== из встроенного скрипта (./demo/script-to-inject.js) =====');
var div = document.createElement('div');
div.style.cssText = ''.concat(
	'border: thick solid tomato;',
	'background: salmon;',
	'width: 400px;',
	'padding: 10px;',
	'color: #fff;',
);
div.textContent = 'Скрипт подключен (./demo/script-to-inject.js)';
document.body.appendChild(div);