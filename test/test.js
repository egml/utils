import * as utils from '../utils';

// var cross = `<symbol viewBox="0 0 100 100" id="cross"><path d="M 20,80 80,20"/><path d="M 20,20 80,80"/></symbol>`;

// var lock = "<symbol id=\"lock\" viewBox=\"0 0 100 100\"><path d=\"M 50,0 C 33.564856,0 20.153675,13.411181 20.153675,29.846327 v 12.143715 h -5.140368 c -4.665586,0 -8.4199,3.75646 -8.4199,8.422046 v 41.165865 c 0,4.665585 3.754314,8.422047 8.4199,8.422047 h 69.973386 c 4.665585,0 8.4199,-3.756462 8.4199,-8.422047 V 50.412087 c 0,-4.665585 -3.754315,-8.422046 -8.4199,-8.422046 H 79.846325 V 29.846327 C 79.846325,13.411181 66.435145,0 50,0 Z m 0,11.538461 c 10.24242,0 18.307863,8.065444 18.307863,18.307866 V 41.990042 H 31.692137 V 29.846327 C 31.692137,19.603905 39.75758,11.538461 50,11.538461 Z m 0,43.902389 a 8.0728139,8.0683019 0 0 1 8.072201,8.070053 8.0728139,8.0683019 0 0 1 -2.30297,5.631867 v 11.639339 c 0,3.194365 -2.573077,5.767084 -5.769231,5.767084 -3.196154,0 -5.769231,-2.572719 -5.769231,-5.767084 V 69.151354 A 8.0728139,8.0683019 0 0 1 41.927799,63.510903 8.0728139,8.0683019 0 0 1 50,55.44085 Z\"/></symbol>";

// // Скрипт:
// utils.injectScript('./script-to-inject.js', function() {
// 	alert('Скрипт загружен');
// });

// // Стиль:
// utils.injectStyle('./style-to-inject.css', function() {
// 	alert('Стиль загружен');
// });

document.addEventListener('DOMContentLoaded', function() {
	var log = document.getElementById('log');

	// utils.injectHtml('<div style="background:lightgreen; padding:.5em; border:thick solid limegreen; margin:2em 0;">Вещество, в отличие от классического случая, трансформирует луч.</div>');

	// utils.injectHtml({
	// 	html: '<div style="background:lightgreen; padding:.5em; border:thick solid limegreen; margin:2em 0;">Вещество, в отличие от классического случая, трансформирует луч.</div>',
	// });

	// utils.injectHtml({
	// 	html: '<div style="background:lightgreen; padding:.5em; border:thick solid limegreen; margin:2em 0;">Вещество, в отличие от классического случая, трансформирует луч.</div>',
	// 	onLoadCallback: function() {
	// 		alert('done');
	// 	},
	// });

	utils.injectHtml({
		url: './partial.html',
		mountElement: document.getElementById('text'),
		onLoadCallback: function() {
			log.textContent += '========== загружено ==========\n';
		},
	});

	// utils.injectHtml({
	// 	url: './partial.html',
	// 	legacy: true,
	// 	onLoadCallback: function() {
	// 		log.textContent += '========== загружено ==========\n';
	// 	},
	// });

	// utils.injectSvgSymbol(cross);
	// utils.injectSvgSymbol(lock);

	// -------------------------------------------
	//  Скрипт:
	// -------------------------------------------

	// utils.injectScript({
	// 	url: './script-to-inject.js',
	// });

	// utils.injectScript({
	// 	url: './script-to-inject.js',
	// 	inline: true,
	// 	mountElement: document.body,
	// 	onLoadCallback: function() {
	// 		alert('Скрипт загружен');
	// 	},
	// });

	// utils.injectScript({
	// 	content: 'alert(\'asd\')',
	// });

	// -------------------------------------------
	//  Стиль:
	// -------------------------------------------

	// utils.injectStyle({
	// 	url: './style-to-inject.css',
	// });

	// utils.injectStyle({
	// 	url: './style-to-inject.css',
	// 	inline: true,
	// 	mountElement: document.body,
	// 	onLoadCallback: function() {
	// 		alert('Стиль загружен');
	// 	},
	// });

});
