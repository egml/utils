/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./demo/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./demo/main.js":
/*!**********************!*\
  !*** ./demo/main.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils */ "./utils.js");


// utils.injectStyle('./style-to-inject.css');
// utils.injectStyle('./style-to-inject.css', true);

// utils.injectScript('./script-to-inject.js');
_utils__WEBPACK_IMPORTED_MODULE_0__["injectScript"]('./script-to-inject.js', true);

/***/ }),

/***/ "./utils.js":
/*!******************!*\
  !*** ./utils.js ***!
  \******************/
/*! exports provided: request, importSvg, importSVG, extend, childByClass, pageScroll, closestParentByClass, isObject, rem, contentBoxHeight, marginBoxHeight, createWidgets, stringCase, injectStyle, injectScript */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "request", function() { return request; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "importSvg", function() { return importSvg; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "importSVG", function() { return importSVG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "extend", function() { return extend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "childByClass", function() { return childByClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pageScroll", function() { return pageScroll; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "closestParentByClass", function() { return closestParentByClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isObject", function() { return isObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rem", function() { return rem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "contentBoxHeight", function() { return contentBoxHeight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "marginBoxHeight", function() { return marginBoxHeight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createWidgets", function() { return createWidgets; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "stringCase", function() { return stringCase; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "injectStyle", function() { return injectStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "injectScript", function() { return injectScript; });
// /*
//  ██████  ███████  ██████  ██    ██ ███████ ███████ ████████
//  ██   ██ ██      ██    ██ ██    ██ ██      ██         ██
//  ██████  █████   ██    ██ ██    ██ █████   ███████    ██
//  ██   ██ ██      ██ ▄▄ ██ ██    ██ ██           ██    ██
//  ██   ██ ███████  ██████   ██████  ███████ ███████    ██
//                      ▀▀
// */
// #request
function request(options) {
	var xhr = new XMLHttpRequest();
	options.type = options.type || 'get';
	xhr.open(options.type, options.url);
	// Yii проверяет наличие хедера X-Requested-With в фильтре ajaxOnly
	// http://www.yiiframework.com/doc/api/1.1/CHttpRequest#getIsAjaxRequest-detail
	xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');
	xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
	xhr.onload = options.success;
	xhr.send(options.query);
};

// /*
//  ██ ███    ███ ██████   ██████  ██████  ████████
//  ██ ████  ████ ██   ██ ██    ██ ██   ██    ██
//  ██ ██ ████ ██ ██████  ██    ██ ██████     ██
//  ██ ██  ██  ██ ██      ██    ██ ██   ██    ██
//  ██ ██      ██ ██       ██████  ██   ██    ██
// 
//  ███████ ██    ██  ██████
//  ██      ██    ██ ██
//  ███████ ██    ██ ██   ███
//       ██  ██  ██  ██    ██
//  ███████   ████    ██████
// */
// #import #svg
function importSvg(url, loadEventName, target, idAttribute) {
	importSVG(url, loadEventName, target, idAttribute);
}

function importSVG(url, loadEventName, target, idAttribute) {
	loadEventName = loadEventName || 'svgload';
	target = target || document.body;
	if (typeof DOMParser !== 'undefined') // IE6-
	request({
		url: url,
		success: function() {
			// Для IE7-9, т.к. они не поддерживают встроенный парсинг свойства responseXML
			// http://msdn.microsoft.com/en-us/library/ie/ms535874%28v=vs.85%29.aspx
			var parser = new DOMParser();
			var xml = parser.parseFromString(this.responseText, 'text/xml');
			// var xml = this.responseXML;
			var svg = document.importNode(xml.documentElement, true);
			if (idAttribute) {
				svg.setAttribute('id', idAttribute);
			}
			target.appendChild(svg);
			target.dispatchEvent(new CustomEvent(loadEventName, { bubbles: true }));
		},
	});
};

// /*
//  ███████ ██   ██ ████████ ███████ ███    ██ ██████
//  ██       ██ ██     ██    ██      ████   ██ ██   ██
//  █████     ███      ██    █████   ██ ██  ██ ██   ██
//  ██       ██ ██     ██    ██      ██  ██ ██ ██   ██
//  ███████ ██   ██    ██    ███████ ██   ████ ██████
// */
// #extend
function extend(dest, source) {
	for (var prop in source) {
		dest[prop] = source[prop];
	}
	return dest;
}

// /*
//   ██████ ██   ██ ██ ██      ██████
//  ██      ██   ██ ██ ██      ██   ██
//  ██      ███████ ██ ██      ██   ██
//  ██      ██   ██ ██ ██      ██   ██
//   ██████ ██   ██ ██ ███████ ██████
// 
//  ██████  ██    ██
//  ██   ██  ██  ██
//  ██████    ████
//  ██   ██    ██
//  ██████     ██
// 
//   ██████ ██       █████  ███████ ███████
//  ██      ██      ██   ██ ██      ██
//  ██      ██      ███████ ███████ ███████
//  ██      ██      ██   ██      ██      ██
//   ██████ ███████ ██   ██ ███████ ███████
// */
// #child #class
function childByClass(parent, className) {
	var children = parent.children;
	for (var i = 0; i < children.length; i++) {
		if (children[i].classList.contains(className)) {
			return children[i];
		}
	}
}

// /*
//  ██████   █████   ██████  ███████
//  ██   ██ ██   ██ ██       ██
//  ██████  ███████ ██   ███ █████
//  ██      ██   ██ ██    ██ ██
//  ██      ██   ██  ██████  ███████
// 
//  ███████  ██████ ██████   ██████  ██      ██
//  ██      ██      ██   ██ ██    ██ ██      ██
//  ███████ ██      ██████  ██    ██ ██      ██
//       ██ ██      ██   ██ ██    ██ ██      ██
//  ███████  ██████ ██   ██  ██████  ███████ ███████
// */
// #page #scroll
// Кросс-браузерное определение величины скролла
function pageScroll() {
	if (window.pageXOffset != undefined) {
		return {
			left: pageXOffset,
			top: pageYOffset
		};
	} else {
		var html = document.documentElement;
		var body = document.body;

		var top = html.scrollTop || body && body.scrollTop || 0;
		top -= html.clientTop;

		var left = html.scrollLeft || body && body.scrollLeft || 0;
		left -= html.clientLeft;

		return {
			left: left,
			top: top
		};
	}
}

// /*
//   ██████ ██       ██████  ███████ ███████ ███████ ████████
//  ██      ██      ██    ██ ██      ██      ██         ██
//  ██      ██      ██    ██ ███████ █████   ███████    ██
//  ██      ██      ██    ██      ██ ██           ██    ██
//   ██████ ███████  ██████  ███████ ███████ ███████    ██
// 
//  ██████   █████  ██████  ███████ ███    ██ ████████
//  ██   ██ ██   ██ ██   ██ ██      ████   ██    ██
//  ██████  ███████ ██████  █████   ██ ██  ██    ██
//  ██      ██   ██ ██   ██ ██      ██  ██ ██    ██
//  ██      ██   ██ ██   ██ ███████ ██   ████    ██
// 
//  ██████  ██    ██
//  ██   ██  ██  ██
//  ██████    ████
//  ██   ██    ██
//  ██████     ██
// 
//   ██████ ██       █████  ███████ ███████
//  ██      ██      ██   ██ ██      ██
//  ██      ██      ███████ ███████ ███████
//  ██      ██      ██   ██      ██      ██
//   ██████ ███████ ██   ██ ███████ ███████
// */
// #closest #parent #class
function closestParentByClass(element, className) {
	var parent = element.parentNode;
	while (!parent.classList.contains(className)) {
		parent = parent.parentNode;
	}
	return parent;
}

// /*
//  ██ ███████
//  ██ ██
//  ██ ███████
//  ██      ██
//  ██ ███████
// 
//   ██████  ██████       ██ ███████  ██████ ████████
//  ██    ██ ██   ██      ██ ██      ██         ██
//  ██    ██ ██████       ██ █████   ██         ██
//  ██    ██ ██   ██ ██   ██ ██      ██         ██
//   ██████  ██████   █████  ███████  ██████    ██
// */
// #is #object
// Сперто из interact.js (сперто немного, но все равно: http://interactjs.io/)
function isObject(thing) {
	return !!thing && (typeof thing === 'object');
}

// /*
//  ██████  ███████ ███    ███
//  ██   ██ ██      ████  ████
//  ██████  █████   ██ ████ ██
//  ██   ██ ██      ██  ██  ██
//  ██   ██ ███████ ██      ██
// */
// #rem
// Подсчёт величины в rem. На входе пиксели без `px`, на выходе величина в rem с `rem`
var rootFontSize;
function rem(pxNoUnits, recalc) {
	if (typeof rootFontSize != 'number' || recalc) {
		// #CBFIX: Edge (42.17134.1.0, EdgeHTML 17.17134) и IE определяют значение как, например, 9.93 в результате >> операция дает 9, поэтому тут дополнительно надо округлять. UPD: Зачем переводить в integer с помощью >>, если мы уже применям к строке Math.round, тем самым она автоматически кастуется в integer.
		// rootFontSize = Math.round(window.getComputedStyle(document.documentElement).getPropertyValue('font-size').slice(0,-2)) >> 0;
		rootFontSize = Math.round(window.getComputedStyle(document.documentElement).getPropertyValue('font-size').slice(0,-2));
	}
	return pxNoUnits/rootFontSize + 'rem';
}

// /*
//   ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████
//  ██      ██    ██ ████   ██    ██    ██      ████   ██    ██
//  ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██
//  ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██
//   ██████  ██████  ██   ████    ██    ███████ ██   ████    ██
// 
//  ██████   ██████  ██   ██
//  ██   ██ ██    ██  ██ ██
//  ██████  ██    ██   ███
//  ██   ██ ██    ██  ██ ██
//  ██████   ██████  ██   ██
// 
//  ██   ██ ███████ ██  ██████  ██   ██ ████████
//  ██   ██ ██      ██ ██       ██   ██    ██
//  ███████ █████   ██ ██   ███ ███████    ██
//  ██   ██ ██      ██ ██    ██ ██   ██    ██
//  ██   ██ ███████ ██  ██████  ██   ██    ██
// */
// #content #box #height
// Подсчёт высоты content-box с дробью (в отличие от $(...).height())
function contentBoxHeight(element) {
	if (element instanceof jQuery) element = element.get(0);
	var style = window.getComputedStyle(element);
	return element.getBoundingClientRect().height
			- style.getPropertyValue('padding-top').slice(0,-2)
			- style.getPropertyValue('padding-bottom').slice(0,-2)
			- style.getPropertyValue('border-top-width').slice(0,-2)
			- style.getPropertyValue('border-bottom-width').slice(0,-2);
}

// /*
//  ███    ███  █████  ██████   ██████  ██ ███    ██
//  ████  ████ ██   ██ ██   ██ ██       ██ ████   ██
//  ██ ████ ██ ███████ ██████  ██   ███ ██ ██ ██  ██
//  ██  ██  ██ ██   ██ ██   ██ ██    ██ ██ ██  ██ ██
//  ██      ██ ██   ██ ██   ██  ██████  ██ ██   ████
// 
//  ██████   ██████  ██   ██
//  ██   ██ ██    ██  ██ ██
//  ██████  ██    ██   ███
//  ██   ██ ██    ██  ██ ██
//  ██████   ██████  ██   ██
// 
//  ██   ██ ███████ ██  ██████  ██   ██ ████████
//  ██   ██ ██      ██ ██       ██   ██    ██
//  ███████ █████   ██ ██   ███ ███████    ██
//  ██   ██ ██      ██ ██    ██ ██   ██    ██
//  ██   ██ ███████ ██  ██████  ██   ██    ██
// */
// #margin #box #height
function marginBoxHeight(element, sourceWindow) {
	sourceWindow = sourceWindow || window;
	var style;
	if (element && (style = sourceWindow.getComputedStyle(element))) {
		return element.getBoundingClientRect().height
				+ (style.getPropertyValue('margin-top').slice(0,-2) >> 0)
				+ (style.getPropertyValue('margin-bottom').slice(0,-2) >> 0);
	} else {
		return 0;
	}
}

// /*
//   ██████ ██████  ███████  █████  ████████ ███████
//  ██      ██   ██ ██      ██   ██    ██    ██
//  ██      ██████  █████   ███████    ██    █████
//  ██      ██   ██ ██      ██   ██    ██    ██
//   ██████ ██   ██ ███████ ██   ██    ██    ███████
// 
//  ██     ██ ██ ██████   ██████  ███████ ████████ ███████
//  ██     ██ ██ ██   ██ ██       ██         ██    ██
//  ██  █  ██ ██ ██   ██ ██   ███ █████      ██    ███████
//  ██ ███ ██ ██ ██   ██ ██    ██ ██         ██         ██
//   ███ ███  ██ ██████   ██████  ███████    ██    ███████
// */
// #create #widgets
/**
 * Создает объекты виджета в определенном контексте.
 * 
 * @param {function|class} constructor Конструктор виджета
 * @param {DOMElement} context В контексте (внутри) какого элемента производить поиск 
 * элементов для инициализации виджета. Если не указан, используется document.
 * @param {object} options Конфигурационный объект, передаваемый создаваемому виджету
 * @param {string} selector Какой селектор использовать для поиска элементов. 
 * Если не указать, используется name.css из прототипа виджета.
 */
function createWidgets(constructor, context, options, selector)
{
	if (typeof constructor == 'undefined') {
		throw new Error('Нужно обязательно указать класс виджета');
	}
	context = context || document;
	selector = selector || '.' + constructor.prototype.name.css;

	var elements = context.querySelectorAll(selector);
	if (elements.length) {
		for (var i = 0; i < elements.length; i++) {
			// Почему-то try...catch выдает куда меньше полезной информации, чем простая обработка ошибок в консоли браузера
			// try {
				new constructor(elements[i], options);
			// } catch(error) {
			// 	console.error(error);
			// }
		}
	} else {
		throw new Error('Элементы в DOM в выбранном контексте по такому селектору не найдены');
	}
}

// /*
//  ███████ ████████ ██████  ██ ███    ██  ██████
//  ██         ██    ██   ██ ██ ████   ██ ██
//  ███████    ██    ██████  ██ ██ ██  ██ ██   ███
//       ██    ██    ██   ██ ██ ██  ██ ██ ██    ██
//  ███████    ██    ██   ██ ██ ██   ████  ██████
// 
//   ██████  █████  ███████ ███████
//  ██      ██   ██ ██      ██
//  ██      ███████ ███████ █████
//  ██      ██   ██      ██ ██
//   ██████ ██   ██ ███████ ███████
// */
// #string #case
function stringCase(bits, type) {
	if (!bits || !bits.length) {
		return '';
	}
	if (typeof bits == 'string') {
		bits = [ bits ];
	}
	var string =  '';
	switch (type) {
		case 'camel':
			bits.forEach(function(bit, i) {
				if (i == 0) {
					string += bit.firstLowerCase();
				} else {
					string += bit.firstUpperCase();
				}
			});
			break;

		case 'capitalCamel':
			bits.forEach(function(bit) {
				string += bit.firstUpperCase();
			});
			break;

		case 'flat':
			string = bits.join('');
			break;

		case 'snake':
			string = bits.join('_');
			break;

		case 'kebab':
			string = bits.join('-');
			break;

		case 'dot':
			string = bits.join('.');
			break;
	
		case 'asIs':
		default:
			string = bits.join('');
			break;
	}
	return string;
}

// /*
//  ███████ ██ ██████  ███████ ████████
//  ██      ██ ██   ██ ██         ██
//  █████   ██ ██████  ███████    ██
//  ██      ██ ██   ██      ██    ██
//  ██      ██ ██   ██ ███████    ██
// 
//  ██    ██ ██████  ██████  ███████ ██████      ██
//  ██    ██ ██   ██ ██   ██ ██      ██   ██    ██
//  ██    ██ ██████  ██████  █████   ██████    ██
//  ██    ██ ██      ██      ██      ██   ██  ██
//   ██████  ██      ██      ███████ ██   ██ ██
// 
//  ██       ██████  ██     ██ ███████ ██████
//  ██      ██    ██ ██     ██ ██      ██   ██
//  ██      ██    ██ ██  █  ██ █████   ██████
//  ██      ██    ██ ██ ███ ██ ██      ██   ██
//  ███████  ██████   ███ ███  ███████ ██   ██
// 
//   ██████  █████  ███████ ███████
//  ██      ██   ██ ██      ██
//  ██      ███████ ███████ █████
//  ██      ██   ██      ██ ██
//   ██████ ██   ██ ███████ ███████
// */
// #first #upper #lower #case
String.prototype.firstUpperCase = function() {
	return this[0].toUpperCase() + this.slice(1);
}
String.prototype.firstLowerCase = function() {
	return this[0].toLowerCase() + this.slice(1);
}

// #TODO: Написать функционал логинга переменной или объекта в HTML
// // /*
// //  ██████   ██████  ███    ███
// //  ██   ██ ██    ██ ████  ████
// //  ██   ██ ██    ██ ██ ████ ██
// //  ██   ██ ██    ██ ██  ██  ██
// //  ██████   ██████  ██      ██
// // 
// //  ██       ██████   ██████
// //  ██      ██    ██ ██
// //  ██      ██    ██ ██   ███
// //  ██      ██    ██ ██    ██
// //  ███████  ██████   ██████
// // */
// // #dom #log
// export function domLog(object, target) {
// 	if (typeof target == 'undefined') {
// 		target = document;
// 	}
// 	var output = '';
// 	if (typeof object == 'object') {
// 		for (var key in object) {
// 			if (object.hasOwnProperty(key)) {
// 				var element = object[key];
// 				output += '';
// 			}
// 		}
// 	}
// 	'<div style="font-family:monospace; font-size:1rem; white-space:nowrap;">' + 
// 	'width &nbsp;= ' + rect.width + '<br>' +
// 	'height = ' + rect.height + '<br>' +
// 	'x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= ' + rect.x + '<br>' +
// 	'y &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= ' + rect.y + '<br>' +
// 	'left &nbsp;&nbsp;= ' + rect.left + '<br>' +
// 	'top &nbsp;&nbsp;&nbsp;= ' + rect.top + '<br>' +
// 	'right &nbsp;= ' + rect.right + '<br>' +
// 	'bottom = ' + rect.bottom + '<br>' +
// 	'</div>';
// }

//	██ ███    ██      ██ ███████  ██████ ████████
//	██ ████   ██      ██ ██      ██         ██
//	██ ██ ██  ██      ██ █████   ██         ██
//	██ ██  ██ ██ ██   ██ ██      ██         ██
//	██ ██   ████  █████  ███████  ██████    ██
// 
//	███████ ████████ ██    ██ ██      ███████
//	██         ██     ██  ██  ██      ██
//	███████    ██      ████   ██      █████
//	     ██    ██       ██    ██      ██
//	███████    ██       ██    ███████ ███████
// 
// #inject #style
function injectStyle(url, inline) {
	if (inline) {
		var xhr = new XMLHttpRequest();
		xhr.open('get', url);
		xhr.onload = function() {
			var tag = document.createElement('style');
			tag.textContent = this.responseText;
			document.head.appendChild(tag);
		};
		xhr.send();
	} else {
		var tag = document.createElement('link');
		tag.rel = 'stylesheet';
		tag.href = url;
		document.head.appendChild(tag);
	}
}

//	██ ███    ██      ██ ███████  ██████ ████████
//	██ ████   ██      ██ ██      ██         ██
//	██ ██ ██  ██      ██ █████   ██         ██
//	██ ██  ██ ██ ██   ██ ██      ██         ██
//	██ ██   ████  █████  ███████  ██████    ██
// 
//	███████  ██████ ██████  ██ ██████  ████████
//	██      ██      ██   ██ ██ ██   ██    ██
//	███████ ██      ██████  ██ ██████     ██
//	     ██ ██      ██   ██ ██ ██         ██
//	███████  ██████ ██   ██ ██ ██         ██
// 
// #inject #script
function injectScript(url, inline) {
	if (inline) {
		var xhr = new XMLHttpRequest();
		xhr.open('get', url);
		xhr.onload = function() {
			var tag = document.createElement('script');
			tag.textContent = this.responseText;
			document.head.appendChild(tag);
		};
		xhr.send();
	} else {
		var tag = document.createElement('script');
		tag.src = url;
		document.head.appendChild(tag);
	}
}



/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vZGVtby9tYWluLmpzIiwid2VicGFjazovLy8uL3V0aWxzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFrQzs7QUFFbEM7QUFDQTs7QUFFQTtBQUNBLG1EQUFrQixnQzs7Ozs7Ozs7Ozs7O0FDTmxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3RUFBd0U7QUFDeEU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7O0FBRU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdEQUF3RCxnQkFBZ0I7QUFDeEUsR0FBRztBQUNILEVBQUU7QUFDRjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0EsZ0JBQWdCLHFCQUFxQjtBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsZUFBZTtBQUMxQixXQUFXLFdBQVc7QUFDdEI7QUFDQSxXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCLHFCQUFxQjtBQUN0QztBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLElBQUk7QUFDSjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1Q0FBdUMsZ0JBQWdCLG9CQUFvQjtBQUMzRSxpQkFBaUI7QUFDakI7QUFDQSxhQUFhLE1BQU0sTUFBTSxNQUFNLE1BQU07QUFDckMsYUFBYSxNQUFNLE1BQU0sTUFBTSxNQUFNO0FBQ3JDLGdCQUFnQixNQUFNO0FBQ3RCLGVBQWUsTUFBTSxNQUFNO0FBQzNCLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6Im1haW4tYnVpbGQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL2RlbW8vbWFpbi5qc1wiKTtcbiIsImltcG9ydCAqIGFzIHV0aWxzIGZyb20gJy4uL3V0aWxzJztcclxuXHJcbi8vIHV0aWxzLmluamVjdFN0eWxlKCcuL3N0eWxlLXRvLWluamVjdC5jc3MnKTtcclxuLy8gdXRpbHMuaW5qZWN0U3R5bGUoJy4vc3R5bGUtdG8taW5qZWN0LmNzcycsIHRydWUpO1xyXG5cclxuLy8gdXRpbHMuaW5qZWN0U2NyaXB0KCcuL3NjcmlwdC10by1pbmplY3QuanMnKTtcclxudXRpbHMuaW5qZWN0U2NyaXB0KCcuL3NjcmlwdC10by1pbmplY3QuanMnLCB0cnVlKTsiLCIvLyAvKlxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paIICAg4paI4paIICAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilogg4paE4paEIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiCAgICAgICAgICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyAgICAgICAgICAgICAgICAgICAgICDiloDiloBcclxuLy8gKi9cclxuLy8gI3JlcXVlc3RcclxuZXhwb3J0IGZ1bmN0aW9uIHJlcXVlc3Qob3B0aW9ucykge1xyXG5cdHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuXHRvcHRpb25zLnR5cGUgPSBvcHRpb25zLnR5cGUgfHwgJ2dldCc7XHJcblx0eGhyLm9wZW4ob3B0aW9ucy50eXBlLCBvcHRpb25zLnVybCk7XHJcblx0Ly8gWWlpINC/0YDQvtCy0LXRgNGP0LXRgiDQvdCw0LvQuNGH0LjQtSDRhdC10LTQtdGA0LAgWC1SZXF1ZXN0ZWQtV2l0aCDQsiDRhNC40LvRjNGC0YDQtSBhamF4T25seVxyXG5cdC8vIGh0dHA6Ly93d3cueWlpZnJhbWV3b3JrLmNvbS9kb2MvYXBpLzEuMS9DSHR0cFJlcXVlc3QjZ2V0SXNBamF4UmVxdWVzdC1kZXRhaWxcclxuXHR4aHIuc2V0UmVxdWVzdEhlYWRlcignWC1SZXF1ZXN0ZWQtV2l0aCcsJ1hNTEh0dHBSZXF1ZXN0Jyk7XHJcblx0eGhyLnNldFJlcXVlc3RIZWFkZXIoJ0NvbnRlbnQtVHlwZScsJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZDsgY2hhcnNldD1VVEYtOCcpO1xyXG5cdHhoci5vbmxvYWQgPSBvcHRpb25zLnN1Y2Nlc3M7XHJcblx0eGhyLnNlbmQob3B0aW9ucy5xdWVyeSk7XHJcbn07XHJcblxyXG4vLyAvKlxyXG4vLyAg4paI4paIIOKWiOKWiOKWiCAgICDilojilojilogg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCDilojilogg4paI4paI4paI4paIIOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paIICAgICDilojilohcclxuLy8gIOKWiOKWiCDilojiloggIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICAgICAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vIFxyXG4vLyAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiFxyXG4vLyAgICAgICDilojiloggIOKWiOKWiCAg4paI4paIICDilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojiloggICDilojilojilojiloggICAg4paI4paI4paI4paI4paI4paIXHJcbi8vICovXHJcbi8vICNpbXBvcnQgI3N2Z1xyXG5leHBvcnQgZnVuY3Rpb24gaW1wb3J0U3ZnKHVybCwgbG9hZEV2ZW50TmFtZSwgdGFyZ2V0LCBpZEF0dHJpYnV0ZSkge1xyXG5cdGltcG9ydFNWRyh1cmwsIGxvYWRFdmVudE5hbWUsIHRhcmdldCwgaWRBdHRyaWJ1dGUpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gaW1wb3J0U1ZHKHVybCwgbG9hZEV2ZW50TmFtZSwgdGFyZ2V0LCBpZEF0dHJpYnV0ZSkge1xyXG5cdGxvYWRFdmVudE5hbWUgPSBsb2FkRXZlbnROYW1lIHx8ICdzdmdsb2FkJztcclxuXHR0YXJnZXQgPSB0YXJnZXQgfHwgZG9jdW1lbnQuYm9keTtcclxuXHRpZiAodHlwZW9mIERPTVBhcnNlciAhPT0gJ3VuZGVmaW5lZCcpIC8vIElFNi1cclxuXHRyZXF1ZXN0KHtcclxuXHRcdHVybDogdXJsLFxyXG5cdFx0c3VjY2VzczogZnVuY3Rpb24oKSB7XHJcblx0XHRcdC8vINCU0LvRjyBJRTctOSwg0YIu0LouINC+0L3QuCDQvdC1INC/0L7QtNC00LXRgNC20LjQstCw0Y7RgiDQstGB0YLRgNC+0LXQvdC90YvQuSDQv9Cw0YDRgdC40L3QsyDRgdCy0L7QudGB0YLQstCwIHJlc3BvbnNlWE1MXHJcblx0XHRcdC8vIGh0dHA6Ly9tc2RuLm1pY3Jvc29mdC5jb20vZW4tdXMvbGlicmFyeS9pZS9tczUzNTg3NCUyOHY9dnMuODUlMjkuYXNweFxyXG5cdFx0XHR2YXIgcGFyc2VyID0gbmV3IERPTVBhcnNlcigpO1xyXG5cdFx0XHR2YXIgeG1sID0gcGFyc2VyLnBhcnNlRnJvbVN0cmluZyh0aGlzLnJlc3BvbnNlVGV4dCwgJ3RleHQveG1sJyk7XHJcblx0XHRcdC8vIHZhciB4bWwgPSB0aGlzLnJlc3BvbnNlWE1MO1xyXG5cdFx0XHR2YXIgc3ZnID0gZG9jdW1lbnQuaW1wb3J0Tm9kZSh4bWwuZG9jdW1lbnRFbGVtZW50LCB0cnVlKTtcclxuXHRcdFx0aWYgKGlkQXR0cmlidXRlKSB7XHJcblx0XHRcdFx0c3ZnLnNldEF0dHJpYnV0ZSgnaWQnLCBpZEF0dHJpYnV0ZSk7XHJcblx0XHRcdH1cclxuXHRcdFx0dGFyZ2V0LmFwcGVuZENoaWxkKHN2Zyk7XHJcblx0XHRcdHRhcmdldC5kaXNwYXRjaEV2ZW50KG5ldyBDdXN0b21FdmVudChsb2FkRXZlbnROYW1lLCB7IGJ1YmJsZXM6IHRydWUgfSkpO1xyXG5cdFx0fSxcclxuXHR9KTtcclxufTtcclxuXHJcbi8vIC8qXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgICDilojilogg4paI4paIICAgICDilojiloggICAg4paI4paIICAgICAg4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paIICAgICDilojilojiloggICAgICDilojiloggICAg4paI4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAgIOKWiOKWiCDilojiloggICAgIOKWiOKWiCAgICDilojiloggICAgICDilojiloggIOKWiOKWiCDilojilogg4paI4paIICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAqL1xyXG4vLyAjZXh0ZW5kXHJcbmV4cG9ydCBmdW5jdGlvbiBleHRlbmQoZGVzdCwgc291cmNlKSB7XHJcblx0Zm9yICh2YXIgcHJvcCBpbiBzb3VyY2UpIHtcclxuXHRcdGRlc3RbcHJvcF0gPSBzb3VyY2VbcHJvcF07XHJcblx0fVxyXG5cdHJldHVybiBkZXN0O1xyXG59XHJcblxyXG4vLyAvKlxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilohcclxuLy8gXHJcbi8vICDilojilojilojilojilojiloggIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCAg4paI4paIICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilojilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgICAg4paI4paIXHJcbi8vIFxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAgICAg4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAqL1xyXG4vLyAjY2hpbGQgI2NsYXNzXHJcbmV4cG9ydCBmdW5jdGlvbiBjaGlsZEJ5Q2xhc3MocGFyZW50LCBjbGFzc05hbWUpIHtcclxuXHR2YXIgY2hpbGRyZW4gPSBwYXJlbnQuY2hpbGRyZW47XHJcblx0Zm9yICh2YXIgaSA9IDA7IGkgPCBjaGlsZHJlbi5sZW5ndGg7IGkrKykge1xyXG5cdFx0aWYgKGNoaWxkcmVuW2ldLmNsYXNzTGlzdC5jb250YWlucyhjbGFzc05hbWUpKSB7XHJcblx0XHRcdHJldHVybiBjaGlsZHJlbltpXTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbi8vIC8qXHJcbi8vICDilojilojilojilojilojiloggICDilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICAg4paI4paIXHJcbi8vICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilojilogg4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilohcclxuLy8gXHJcbi8vICDilojilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paIICDilojiloggICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAgICAgICDilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAqL1xyXG4vLyAjcGFnZSAjc2Nyb2xsXHJcbi8vINCa0YDQvtGB0YEt0LHRgNCw0YPQt9C10YDQvdC+0LUg0L7Qv9GA0LXQtNC10LvQtdC90LjQtSDQstC10LvQuNGH0LjQvdGLINGB0LrRgNC+0LvQu9CwXHJcbmV4cG9ydCBmdW5jdGlvbiBwYWdlU2Nyb2xsKCkge1xyXG5cdGlmICh3aW5kb3cucGFnZVhPZmZzZXQgIT0gdW5kZWZpbmVkKSB7XHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHRsZWZ0OiBwYWdlWE9mZnNldCxcclxuXHRcdFx0dG9wOiBwYWdlWU9mZnNldFxyXG5cdFx0fTtcclxuXHR9IGVsc2Uge1xyXG5cdFx0dmFyIGh0bWwgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XHJcblx0XHR2YXIgYm9keSA9IGRvY3VtZW50LmJvZHk7XHJcblxyXG5cdFx0dmFyIHRvcCA9IGh0bWwuc2Nyb2xsVG9wIHx8IGJvZHkgJiYgYm9keS5zY3JvbGxUb3AgfHwgMDtcclxuXHRcdHRvcCAtPSBodG1sLmNsaWVudFRvcDtcclxuXHJcblx0XHR2YXIgbGVmdCA9IGh0bWwuc2Nyb2xsTGVmdCB8fCBib2R5ICYmIGJvZHkuc2Nyb2xsTGVmdCB8fCAwO1xyXG5cdFx0bGVmdCAtPSBodG1sLmNsaWVudExlZnQ7XHJcblxyXG5cdFx0cmV0dXJuIHtcclxuXHRcdFx0bGVmdDogbGVmdCxcclxuXHRcdFx0dG9wOiB0b3BcclxuXHRcdH07XHJcblx0fVxyXG59XHJcblxyXG4vLyAvKlxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAgICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojiloggICDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICAgICAgICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vIFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paI4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggICDilojilogg4paI4paIICDilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gXHJcbi8vICDilojilojilojilojilojiloggIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCAg4paI4paIICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilojilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgICAg4paI4paIXHJcbi8vIFxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAgICAg4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAqL1xyXG4vLyAjY2xvc2VzdCAjcGFyZW50ICNjbGFzc1xyXG5leHBvcnQgZnVuY3Rpb24gY2xvc2VzdFBhcmVudEJ5Q2xhc3MoZWxlbWVudCwgY2xhc3NOYW1lKSB7XHJcblx0dmFyIHBhcmVudCA9IGVsZW1lbnQucGFyZW50Tm9kZTtcclxuXHR3aGlsZSAoIXBhcmVudC5jbGFzc0xpc3QuY29udGFpbnMoY2xhc3NOYW1lKSkge1xyXG5cdFx0cGFyZW50ID0gcGFyZW50LnBhcmVudE5vZGU7XHJcblx0fVxyXG5cdHJldHVybiBwYXJlbnQ7XHJcbn1cclxuXHJcbi8vIC8qXHJcbi8vICDilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojilogg4paI4paIXHJcbi8vICDilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gXHJcbi8vICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICAgICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojiloggICAgICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgICAgICDilojilogg4paI4paI4paI4paI4paIICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyAqL1xyXG4vLyAjaXMgI29iamVjdFxyXG4vLyDQodC/0LXRgNGC0L4g0LjQtyBpbnRlcmFjdC5qcyAo0YHQv9C10YDRgtC+INC90LXQvNC90L7Qs9C+LCDQvdC+INCy0YHQtSDRgNCw0LLQvdC+OiBodHRwOi8vaW50ZXJhY3Rqcy5pby8pXHJcbmV4cG9ydCBmdW5jdGlvbiBpc09iamVjdCh0aGluZykge1xyXG5cdHJldHVybiAhIXRoaW5nICYmICh0eXBlb2YgdGhpbmcgPT09ICdvYmplY3QnKTtcclxufVxyXG5cclxuLy8gLypcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiCAgICDilojilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilojilojiloggIOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggICDilojilogg4paI4paI4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAg4paI4paIICDilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICovXHJcbi8vICNyZW1cclxuLy8g0J/QvtC00YHRh9GR0YIg0LLQtdC70LjRh9C40L3RiyDQsiByZW0uINCd0LAg0LLRhdC+0LTQtSDQv9C40LrRgdC10LvQuCDQsdC10LcgYHB4YCwg0L3QsCDQstGL0YXQvtC00LUg0LLQtdC70LjRh9C40L3QsCDQsiByZW0g0YEgYHJlbWBcclxudmFyIHJvb3RGb250U2l6ZTtcclxuZXhwb3J0IGZ1bmN0aW9uIHJlbShweE5vVW5pdHMsIHJlY2FsYykge1xyXG5cdGlmICh0eXBlb2Ygcm9vdEZvbnRTaXplICE9ICdudW1iZXInIHx8IHJlY2FsYykge1xyXG5cdFx0Ly8gI0NCRklYOiBFZGdlICg0Mi4xNzEzNC4xLjAsIEVkZ2VIVE1MIDE3LjE3MTM0KSDQuCBJRSDQvtC/0YDQtdC00LXQu9GP0Y7RgiDQt9C90LDRh9C10L3QuNC1INC60LDQuiwg0L3QsNC/0YDQuNC80LXRgCwgOS45MyDQsiDRgNC10LfRg9C70YzRgtCw0YLQtSA+PiDQvtC/0LXRgNCw0YbQuNGPINC00LDQtdGCIDksINC/0L7RjdGC0L7QvNGDINGC0YPRgiDQtNC+0L/QvtC70L3QuNGC0LXQu9GM0L3QviDQvdCw0LTQviDQvtC60YDRg9Cz0LvRj9GC0YwuIFVQRDog0JfQsNGH0LXQvCDQv9C10YDQtdCy0L7QtNC40YLRjCDQsiBpbnRlZ2VyINGBINC/0L7QvNC+0YnRjNGOID4+LCDQtdGB0LvQuCDQvNGLINGD0LbQtSDQv9GA0LjQvNC10L3Rj9C8INC6INGB0YLRgNC+0LrQtSBNYXRoLnJvdW5kLCDRgtC10Lwg0YHQsNC80YvQvCDQvtC90LAg0LDQstGC0L7QvNCw0YLQuNGH0LXRgdC60Lgg0LrQsNGB0YLRg9C10YLRgdGPINCyIGludGVnZXIuXHJcblx0XHQvLyByb290Rm9udFNpemUgPSBNYXRoLnJvdW5kKHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCkuZ2V0UHJvcGVydHlWYWx1ZSgnZm9udC1zaXplJykuc2xpY2UoMCwtMikpID4+IDA7XHJcblx0XHRyb290Rm9udFNpemUgPSBNYXRoLnJvdW5kKHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCkuZ2V0UHJvcGVydHlWYWx1ZSgnZm9udC1zaXplJykuc2xpY2UoMCwtMikpO1xyXG5cdH1cclxuXHRyZXR1cm4gcHhOb1VuaXRzL3Jvb3RGb250U2l6ZSArICdyZW0nO1xyXG59XHJcblxyXG4vLyAvKlxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojiloggICAgICDilojilojilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiCAgICDilojiloggICAg4paI4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggIOKWiOKWiCDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICDilojilohcclxuLy8gICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paI4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojiloggICAg4paI4paIICAg4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilohcclxuLy8gXHJcbi8vICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAqL1xyXG4vLyAjY29udGVudCAjYm94ICNoZWlnaHRcclxuLy8g0J/QvtC00YHRh9GR0YIg0LLRi9GB0L7RgtGLIGNvbnRlbnQtYm94INGBINC00YDQvtCx0YzRjiAo0LIg0L7RgtC70LjRh9C40LUg0L7RgiAkKC4uLikuaGVpZ2h0KCkpXHJcbmV4cG9ydCBmdW5jdGlvbiBjb250ZW50Qm94SGVpZ2h0KGVsZW1lbnQpIHtcclxuXHRpZiAoZWxlbWVudCBpbnN0YW5jZW9mIGpRdWVyeSkgZWxlbWVudCA9IGVsZW1lbnQuZ2V0KDApO1xyXG5cdHZhciBzdHlsZSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQpO1xyXG5cdHJldHVybiBlbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmhlaWdodFxyXG5cdFx0XHQtIHN0eWxlLmdldFByb3BlcnR5VmFsdWUoJ3BhZGRpbmctdG9wJykuc2xpY2UoMCwtMilcclxuXHRcdFx0LSBzdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKCdwYWRkaW5nLWJvdHRvbScpLnNsaWNlKDAsLTIpXHJcblx0XHRcdC0gc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnYm9yZGVyLXRvcC13aWR0aCcpLnNsaWNlKDAsLTIpXHJcblx0XHRcdC0gc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnYm9yZGVyLWJvdHRvbS13aWR0aCcpLnNsaWNlKDAsLTIpO1xyXG59XHJcblxyXG4vLyAvKlxyXG4vLyAg4paI4paI4paIICAgIOKWiOKWiOKWiCAg4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiCDilojilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojiloggIOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgICDilojilogg4paI4paI4paI4paIICAg4paI4paIXHJcbi8vICDilojilogg4paI4paI4paI4paIIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilojilogg4paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiFxyXG4vLyAg4paI4paIICDilojiloggIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojiloggICAg4paI4paIICAg4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilohcclxuLy8gXHJcbi8vICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAqL1xyXG4vLyAjbWFyZ2luICNib3ggI2hlaWdodFxyXG5leHBvcnQgZnVuY3Rpb24gbWFyZ2luQm94SGVpZ2h0KGVsZW1lbnQsIHNvdXJjZVdpbmRvdykge1xyXG5cdHNvdXJjZVdpbmRvdyA9IHNvdXJjZVdpbmRvdyB8fCB3aW5kb3c7XHJcblx0dmFyIHN0eWxlO1xyXG5cdGlmIChlbGVtZW50ICYmIChzdHlsZSA9IHNvdXJjZVdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQpKSkge1xyXG5cdFx0cmV0dXJuIGVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0XHJcblx0XHRcdFx0KyAoc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnbWFyZ2luLXRvcCcpLnNsaWNlKDAsLTIpID4+IDApXHJcblx0XHRcdFx0KyAoc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnbWFyZ2luLWJvdHRvbScpLnNsaWNlKDAsLTIpID4+IDApO1xyXG5cdH0gZWxzZSB7XHJcblx0XHRyZXR1cm4gMDtcclxuXHR9XHJcbn1cclxuXHJcbi8vIC8qXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggICDilojilojilojilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilohcclxuLy8gXHJcbi8vICDilojiloggICAgIOKWiOKWiCDilojilogg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICDilojilogg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICAg4paI4paIICAgICAgICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICDiloggIOKWiOKWiCDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiCDilojilojilojilojiloggICAgICDilojiloggICAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojilogg4paI4paI4paIIOKWiOKWiCDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAgICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICAg4paI4paI4paIIOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilohcclxuLy8gKi9cclxuLy8gI2NyZWF0ZSAjd2lkZ2V0c1xyXG4vKipcclxuICog0KHQvtC30LTQsNC10YIg0L7QsdGK0LXQutGC0Ysg0LLQuNC00LbQtdGC0LAg0LIg0L7Qv9GA0LXQtNC10LvQtdC90L3QvtC8INC60L7QvdGC0LXQutGB0YLQtS5cclxuICogXHJcbiAqIEBwYXJhbSB7ZnVuY3Rpb258Y2xhc3N9IGNvbnN0cnVjdG9yINCa0L7QvdGB0YLRgNGD0LrRgtC+0YAg0LLQuNC00LbQtdGC0LBcclxuICogQHBhcmFtIHtET01FbGVtZW50fSBjb250ZXh0INCSINC60L7QvdGC0LXQutGB0YLQtSAo0LLQvdGD0YLRgNC4KSDQutCw0LrQvtCz0L4g0Y3Qu9C10LzQtdC90YLQsCDQv9GA0L7QuNC30LLQvtC00LjRgtGMINC/0L7QuNGB0LogXHJcbiAqINGN0LvQtdC80LXQvdGC0L7QsiDQtNC70Y8g0LjQvdC40YbQuNCw0LvQuNC30LDRhtC40Lgg0LLQuNC00LbQtdGC0LAuINCV0YHQu9C4INC90LUg0YPQutCw0LfQsNC9LCDQuNGB0L/QvtC70YzQt9GD0LXRgtGB0Y8gZG9jdW1lbnQuXHJcbiAqIEBwYXJhbSB7b2JqZWN0fSBvcHRpb25zINCa0L7QvdGE0LjQs9GD0YDQsNGG0LjQvtC90L3Ri9C5INC+0LHRitC10LrRgiwg0L/QtdGA0LXQtNCw0LLQsNC10LzRi9C5INGB0L7Qt9C00LDQstCw0LXQvNC+0LzRgyDQstC40LTQttC10YLRg1xyXG4gKiBAcGFyYW0ge3N0cmluZ30gc2VsZWN0b3Ig0JrQsNC60L7QuSDRgdC10LvQtdC60YLQvtGAINC40YHQv9C+0LvRjNC30L7QstCw0YLRjCDQtNC70Y8g0L/QvtC40YHQutCwINGN0LvQtdC80LXQvdGC0L7Qsi4gXHJcbiAqINCV0YHQu9C4INC90LUg0YPQutCw0LfQsNGC0YwsINC40YHQv9C+0LvRjNC30YPQtdGC0YHRjyBuYW1lLmNzcyDQuNC3INC/0YDQvtGC0L7RgtC40L/QsCDQstC40LTQttC10YLQsC5cclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVXaWRnZXRzKGNvbnN0cnVjdG9yLCBjb250ZXh0LCBvcHRpb25zLCBzZWxlY3Rvcilcclxue1xyXG5cdGlmICh0eXBlb2YgY29uc3RydWN0b3IgPT0gJ3VuZGVmaW5lZCcpIHtcclxuXHRcdHRocm93IG5ldyBFcnJvcign0J3Rg9C20L3QviDQvtCx0Y/Qt9Cw0YLQtdC70YzQvdC+INGD0LrQsNC30LDRgtGMINC60LvQsNGB0YEg0LLQuNC00LbQtdGC0LAnKTtcclxuXHR9XHJcblx0Y29udGV4dCA9IGNvbnRleHQgfHwgZG9jdW1lbnQ7XHJcblx0c2VsZWN0b3IgPSBzZWxlY3RvciB8fCAnLicgKyBjb25zdHJ1Y3Rvci5wcm90b3R5cGUubmFtZS5jc3M7XHJcblxyXG5cdHZhciBlbGVtZW50cyA9IGNvbnRleHQucXVlcnlTZWxlY3RvckFsbChzZWxlY3Rvcik7XHJcblx0aWYgKGVsZW1lbnRzLmxlbmd0aCkge1xyXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBlbGVtZW50cy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHQvLyDQn9C+0YfQtdC80YMt0YLQviB0cnkuLi5jYXRjaCDQstGL0LTQsNC10YIg0LrRg9C00LAg0LzQtdC90YzRiNC1INC/0L7Qu9C10LfQvdC+0Lkg0LjQvdGE0L7RgNC80LDRhtC40LgsINGH0LXQvCDQv9GA0L7RgdGC0LDRjyDQvtCx0YDQsNCx0L7RgtC60LAg0L7RiNC40LHQvtC6INCyINC60L7QvdGB0L7Qu9C4INCx0YDQsNGD0LfQtdGA0LBcclxuXHRcdFx0Ly8gdHJ5IHtcclxuXHRcdFx0XHRuZXcgY29uc3RydWN0b3IoZWxlbWVudHNbaV0sIG9wdGlvbnMpO1xyXG5cdFx0XHQvLyB9IGNhdGNoKGVycm9yKSB7XHJcblx0XHRcdC8vIFx0Y29uc29sZS5lcnJvcihlcnJvcik7XHJcblx0XHRcdC8vIH1cclxuXHRcdH1cclxuXHR9IGVsc2Uge1xyXG5cdFx0dGhyb3cgbmV3IEVycm9yKCfQrdC70LXQvNC10L3RgtGLINCyIERPTSDQsiDQstGL0LHRgNCw0L3QvdC+0Lwg0LrQvtC90YLQtdC60YHRgtC1INC/0L4g0YLQsNC60L7QvNGDINGB0LXQu9C10LrRgtC+0YDRgyDQvdC1INC90LDQudC00LXQvdGLJyk7XHJcblx0fVxyXG59XHJcblxyXG4vLyAvKlxyXG4vLyAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiCDilojilojiloggICAg4paI4paIICDilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgICAgIOKWiOKWiCAgICDilojiloggICDilojilogg4paI4paIIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paI4paI4paI4paI4paIICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiFxyXG4vLyAgICAgICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paIICDilojilogg4paI4paIIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paIICAg4paI4paIIOKWiOKWiCDilojiloggICDilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiCDilojilohcclxuLy8gICDilojilojilojilojilojilogg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gKi9cclxuLy8gI3N0cmluZyAjY2FzZVxyXG5leHBvcnQgZnVuY3Rpb24gc3RyaW5nQ2FzZShiaXRzLCB0eXBlKSB7XHJcblx0aWYgKCFiaXRzIHx8ICFiaXRzLmxlbmd0aCkge1xyXG5cdFx0cmV0dXJuICcnO1xyXG5cdH1cclxuXHRpZiAodHlwZW9mIGJpdHMgPT0gJ3N0cmluZycpIHtcclxuXHRcdGJpdHMgPSBbIGJpdHMgXTtcclxuXHR9XHJcblx0dmFyIHN0cmluZyA9ICAnJztcclxuXHRzd2l0Y2ggKHR5cGUpIHtcclxuXHRcdGNhc2UgJ2NhbWVsJzpcclxuXHRcdFx0Yml0cy5mb3JFYWNoKGZ1bmN0aW9uKGJpdCwgaSkge1xyXG5cdFx0XHRcdGlmIChpID09IDApIHtcclxuXHRcdFx0XHRcdHN0cmluZyArPSBiaXQuZmlyc3RMb3dlckNhc2UoKTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0c3RyaW5nICs9IGJpdC5maXJzdFVwcGVyQ2FzZSgpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdGNhc2UgJ2NhcGl0YWxDYW1lbCc6XHJcblx0XHRcdGJpdHMuZm9yRWFjaChmdW5jdGlvbihiaXQpIHtcclxuXHRcdFx0XHRzdHJpbmcgKz0gYml0LmZpcnN0VXBwZXJDYXNlKCk7XHJcblx0XHRcdH0pO1xyXG5cdFx0XHRicmVhaztcclxuXHJcblx0XHRjYXNlICdmbGF0JzpcclxuXHRcdFx0c3RyaW5nID0gYml0cy5qb2luKCcnKTtcclxuXHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0Y2FzZSAnc25ha2UnOlxyXG5cdFx0XHRzdHJpbmcgPSBiaXRzLmpvaW4oJ18nKTtcclxuXHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0Y2FzZSAna2ViYWInOlxyXG5cdFx0XHRzdHJpbmcgPSBiaXRzLmpvaW4oJy0nKTtcclxuXHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0Y2FzZSAnZG90JzpcclxuXHRcdFx0c3RyaW5nID0gYml0cy5qb2luKCcuJyk7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFxyXG5cdFx0Y2FzZSAnYXNJcyc6XHJcblx0XHRkZWZhdWx0OlxyXG5cdFx0XHRzdHJpbmcgPSBiaXRzLmpvaW4oJycpO1xyXG5cdFx0XHRicmVhaztcclxuXHR9XHJcblx0cmV0dXJuIHN0cmluZztcclxufVxyXG5cclxuLy8gLypcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilogg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggICDilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIICDilojiloggICAgICDilojiloggICAgICDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIIOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiCAgICAgICDilojilojilojilojilojiloggIOKWiOKWiCAgICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAgICDilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCAg4paIICDilojilogg4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCDilojilojilogg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICDilojilojilogg4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIXHJcbi8vIFxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIICAgICAg4paI4paIIOKWiOKWiFxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAqL1xyXG4vLyAjZmlyc3QgI3VwcGVyICNsb3dlciAjY2FzZVxyXG5TdHJpbmcucHJvdG90eXBlLmZpcnN0VXBwZXJDYXNlID0gZnVuY3Rpb24oKSB7XHJcblx0cmV0dXJuIHRoaXNbMF0udG9VcHBlckNhc2UoKSArIHRoaXMuc2xpY2UoMSk7XHJcbn1cclxuU3RyaW5nLnByb3RvdHlwZS5maXJzdExvd2VyQ2FzZSA9IGZ1bmN0aW9uKCkge1xyXG5cdHJldHVybiB0aGlzWzBdLnRvTG93ZXJDYXNlKCkgKyB0aGlzLnNsaWNlKDEpO1xyXG59XHJcblxyXG4vLyAjVE9ETzog0J3QsNC/0LjRgdCw0YLRjCDRhNGD0L3QutGG0LjQvtC90LDQuyDQu9C+0LPQuNC90LPQsCDQv9C10YDQtdC80LXQvdC90L7QuSDQuNC70Lgg0L7QsdGK0LXQutGC0LAg0LIgSFRNTFxyXG4vLyAvLyAvKlxyXG4vLyAvLyAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojilojiloggICAg4paI4paI4paIXHJcbi8vIC8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojilojilojiloggIOKWiOKWiOKWiOKWiFxyXG4vLyAvLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiOKWiOKWiCDilojilohcclxuLy8gLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiCAg4paI4paIICDilojilohcclxuLy8gLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgICAg4paI4paIXHJcbi8vIC8vIFxyXG4vLyAvLyAg4paI4paIICAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAvLyAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojilohcclxuLy8gLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paI4paIXHJcbi8vIC8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCAgICDilojilohcclxuLy8gLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIXHJcbi8vIC8vICovXHJcbi8vIC8vICNkb20gI2xvZ1xyXG4vLyBleHBvcnQgZnVuY3Rpb24gZG9tTG9nKG9iamVjdCwgdGFyZ2V0KSB7XHJcbi8vIFx0aWYgKHR5cGVvZiB0YXJnZXQgPT0gJ3VuZGVmaW5lZCcpIHtcclxuLy8gXHRcdHRhcmdldCA9IGRvY3VtZW50O1xyXG4vLyBcdH1cclxuLy8gXHR2YXIgb3V0cHV0ID0gJyc7XHJcbi8vIFx0aWYgKHR5cGVvZiBvYmplY3QgPT0gJ29iamVjdCcpIHtcclxuLy8gXHRcdGZvciAodmFyIGtleSBpbiBvYmplY3QpIHtcclxuLy8gXHRcdFx0aWYgKG9iamVjdC5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XHJcbi8vIFx0XHRcdFx0dmFyIGVsZW1lbnQgPSBvYmplY3Rba2V5XTtcclxuLy8gXHRcdFx0XHRvdXRwdXQgKz0gJyc7XHJcbi8vIFx0XHRcdH1cclxuLy8gXHRcdH1cclxuLy8gXHR9XHJcbi8vIFx0JzxkaXYgc3R5bGU9XCJmb250LWZhbWlseTptb25vc3BhY2U7IGZvbnQtc2l6ZToxcmVtOyB3aGl0ZS1zcGFjZTpub3dyYXA7XCI+JyArIFxyXG4vLyBcdCd3aWR0aCAmbmJzcDs9ICcgKyByZWN0LndpZHRoICsgJzxicj4nICtcclxuLy8gXHQnaGVpZ2h0ID0gJyArIHJlY3QuaGVpZ2h0ICsgJzxicj4nICtcclxuLy8gXHQneCAmbmJzcDsmbmJzcDsmbmJzcDsmbmJzcDsmbmJzcDs9ICcgKyByZWN0LnggKyAnPGJyPicgK1xyXG4vLyBcdCd5ICZuYnNwOyZuYnNwOyZuYnNwOyZuYnNwOyZuYnNwOz0gJyArIHJlY3QueSArICc8YnI+JyArXHJcbi8vIFx0J2xlZnQgJm5ic3A7Jm5ic3A7PSAnICsgcmVjdC5sZWZ0ICsgJzxicj4nICtcclxuLy8gXHQndG9wICZuYnNwOyZuYnNwOyZuYnNwOz0gJyArIHJlY3QudG9wICsgJzxicj4nICtcclxuLy8gXHQncmlnaHQgJm5ic3A7PSAnICsgcmVjdC5yaWdodCArICc8YnI+JyArXHJcbi8vIFx0J2JvdHRvbSA9ICcgKyByZWN0LmJvdHRvbSArICc8YnI+JyArXHJcbi8vIFx0JzwvZGl2Pic7XHJcbi8vIH1cclxuXHJcbi8vXHTilojilogg4paI4paI4paIICAgIOKWiOKWiCAgICAgIOKWiOKWiCDilojilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCDilojilojilojiloggICDilojiloggICAgICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIIOKWiOKWiCAg4paI4paIICAgICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiCAgICAgICAgIOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIICAg4paI4paI4paI4paIICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICAgICDilojilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCAgICAgICAgIOKWiOKWiCAgICAg4paI4paIICDilojiloggIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vL1x04paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiFxyXG4vL1x0ICAgICDilojiloggICAg4paI4paIICAgICAgIOKWiOKWiCAgICDilojiloggICAgICDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAgICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gXHJcbi8vICNpbmplY3QgI3N0eWxlXHJcbmV4cG9ydCBmdW5jdGlvbiBpbmplY3RTdHlsZSh1cmwsIGlubGluZSkge1xyXG5cdGlmIChpbmxpbmUpIHtcclxuXHRcdHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuXHRcdHhoci5vcGVuKCdnZXQnLCB1cmwpO1xyXG5cdFx0eGhyLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgdGFnID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3R5bGUnKTtcclxuXHRcdFx0dGFnLnRleHRDb250ZW50ID0gdGhpcy5yZXNwb25zZVRleHQ7XHJcblx0XHRcdGRvY3VtZW50LmhlYWQuYXBwZW5kQ2hpbGQodGFnKTtcclxuXHRcdH07XHJcblx0XHR4aHIuc2VuZCgpO1xyXG5cdH0gZWxzZSB7XHJcblx0XHR2YXIgdGFnID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnbGluaycpO1xyXG5cdFx0dGFnLnJlbCA9ICdzdHlsZXNoZWV0JztcclxuXHRcdHRhZy5ocmVmID0gdXJsO1xyXG5cdFx0ZG9jdW1lbnQuaGVhZC5hcHBlbmRDaGlsZCh0YWcpO1xyXG5cdH1cclxufVxyXG5cclxuLy9cdOKWiOKWiCDilojilojiloggICAg4paI4paIICAgICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICAgICDilojilohcclxuLy9cdOKWiOKWiCDilojilogg4paI4paIICDilojiloggICAgICDilojilogg4paI4paI4paI4paI4paIICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIICDilojilogg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICAgICDilojilohcclxuLy9cdOKWiOKWiCDilojiloggICDilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vIFxyXG4vL1x04paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICDilojilogg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vL1x04paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgICAg4paI4paIXHJcbi8vXHQgICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojilogg4paI4paIIOKWiOKWiCAgICAgICAgIOKWiOKWiFxyXG4vL1x04paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCDilojiloggICAgICAgICDilojilohcclxuLy8gXHJcbi8vICNpbmplY3QgI3NjcmlwdFxyXG5leHBvcnQgZnVuY3Rpb24gaW5qZWN0U2NyaXB0KHVybCwgaW5saW5lKSB7XHJcblx0aWYgKGlubGluZSkge1xyXG5cdFx0dmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG5cdFx0eGhyLm9wZW4oJ2dldCcsIHVybCk7XHJcblx0XHR4aHIub25sb2FkID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciB0YWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcclxuXHRcdFx0dGFnLnRleHRDb250ZW50ID0gdGhpcy5yZXNwb25zZVRleHQ7XHJcblx0XHRcdGRvY3VtZW50LmhlYWQuYXBwZW5kQ2hpbGQodGFnKTtcclxuXHRcdH07XHJcblx0XHR4aHIuc2VuZCgpO1xyXG5cdH0gZWxzZSB7XHJcblx0XHR2YXIgdGFnID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XHJcblx0XHR0YWcuc3JjID0gdXJsO1xyXG5cdFx0ZG9jdW1lbnQuaGVhZC5hcHBlbmRDaGlsZCh0YWcpO1xyXG5cdH1cclxufVxyXG5cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==