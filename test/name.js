// #NOTE: Порядок подключения здесь важен. Сначала подключается config, в котором настраивается прототип Name, config выполняется первым. 
import './config';
import Widget from '../Widget';
// import Name from '../Name';

window.buttonAction = function() {
	var c = new Widget(document.querySelector('p'));
	console.log(c.name, c.name.ns);
}

window.addEventListener('load', function() {

	window.a = new Widget(document.querySelector('div'));

	document.querySelector('div.log').innerHTML = 
		'<div style="font-family:monospace; font-size:1rem; white-space:nowrap;">' + 
			'name.toString() = ' + a.name.toString() + '<br>' +
			'name.asIs() = ' + a.name.asIs() + '<br>' +
			'name.asIs(\'SuffiX\') = ' + a.name.asIs('SuffiX') + '<br>' +
			'name.asIs([\'AnotheR\', \'SuffiX\']) = ' + a.name.asIs(['AnotheR', 'SuffiX']) + '<br>' +
			'name.camel() = ' + a.name.camel() + '<br>' +
			'name.camel(\'SuffiX\') = ' + a.name.camel('SuffiX') + '<br>' +
			'name.camel([\'AnotheR\', \'SuffiX\']) = ' + a.name.camel(['AnotheR', 'SuffiX']) + '<br>' +
			'name.capitalCamel() = ' + a.name.capitalCamel() + '<br>' +
			'name.capitalCamel(\'SuffiX\') = ' + a.name.capitalCamel('SuffiX') + '<br>' +
			'name.capitalCamel([\'AnotheR\', \'SuffiX\']) = ' + a.name.capitalCamel(['AnotheR', 'SuffiX']) + '<br>' +
			'name.kebab() = ' + a.name.kebab() + '<br>' +
			'name.kebab(\'SuffiX\') = ' + a.name.kebab('SuffiX') + '<br>' +
			'name.kebab([\'AnotheR\', \'SuffiX\']) = ' + a.name.kebab(['AnotheR', 'SuffiX']) + '<br>' +
			'name.flat() = ' + a.name.flat() + '<br>' +
			'name.flat(\'SuffiX\') = ' + a.name.flat('SuffiX') + '<br>' +
			'name.flat([\'AnotheR\', \'SuffiX\']) = ' + a.name.flat(['AnotheR', 'SuffiX']) + '<br>' +
			'name.dot() = ' + a.name.dot() + '<br>' +
			'name.dot(\'SuffiX\') = ' + a.name.dot('SuffiX') + '<br>' +
			'name.dot([\'AnotheR\', \'SuffiX\']) = ' + a.name.dot(['AnotheR', 'SuffiX']) + '<br>' +
			'name.event([\'evenT\', \'namE\']) = ' + a.name.event(['evenT', 'namE']) + '<br>' +
			'name.event(\'evenT_namE\') = ' + a.name.event('evenT_namE') + '<br>' +
			'name.css() = ' + a.name.css() + '<br>' +
			'name.css(\'SuffiX\') = ' + a.name.css('SuffiX') + '<br>' +
			'name.css([\'AnotheR\', \'SuffiX\']) = ' + a.name.css(['AnotheR', 'SuffiX']) + '<br>' +
			'name.cssClassSelector() = ' + a.name.cssClassSelector() + '<br>' +
			'name.cssClassSelector(\'appendage\') = ' + a.name.cssClassSelector('appendage') + '<br>' +
			'name.cssModificator() = ' + a.name.cssModificator() + '<br>' +
			'name.cssModificator(\'mod\') = ' + a.name.cssModificator('mod') + '<br>' +
			'name.cssModificator([\'anotheR\', \'mod\']) = ' + a.name.cssModificator(['anotheR', 'mod']) + '<br>' +
		'</div>';

	// console.log('name.toString() =', a.name.toString());
	// console.log('name.asIs() =', a.name.asIs());
	// console.log('name.asIs(\'SuffiX\') =', a.name.asIs('SuffiX'));
	// console.log('name.asIs([\'AnotheR\', \'SuffiX\']) =', a.name.asIs(['AnotheR', 'SuffiX']));
	// console.log('name.camel =', a.name.camel);
	// console.log('name.capitalCamel =', a.name.capitalCamel);
	// console.log('name.kebab =', a.name.kebab);
	// console.log('name.flat =', a.name.flat);
	// console.log('name.dot', a.name.dot);
	// console.log('name.event([\'event\', \'name\']) =', a.name.event(['event', 'name']));
	// console.log('name.event(\'event_name\') =', a.name.event('event_name'));
	// console.log('name.css =', a.name.css);
	// console.log('name.cssClass() =', a.name.cssClass());
	// console.log('name.cssClass(\'appendage\') =', a.name.cssClass('appendage'));
	// console.log('name.cssClassSelector() =', a.name.cssClassSelector());
	// console.log('name.cssClassSelector(\'appendage\') =', a.name.cssClassSelector('appendage'));
	// console.log('name.cssModificator() =', a.name.cssModificator());
	// console.log('name.cssModificator(\'mod\') =', a.name.cssModificator('mod'));

	console.log(a);

});