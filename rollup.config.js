export default {
	input: 'test/test.js',
	output: {
		file: 'test/test-build.js',
		format: 'iife',
		sourcemap: 'inline',
	},
	watch: {
		clearScreen: false,
	},
};