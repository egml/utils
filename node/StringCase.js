"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = StringCase;

var utils = _interopRequireWildcard(require("./utils"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function StringCase(bits) {
  this.bits = bits;

  if (Array.isArray(bits)) {
    this.asIs = this.bits.join('');
    bits.forEach(function (bit, i) {
      bits[i] = bit.toLowerCase();
    });
  } else {
    this.asIs = this.bits;
    bits = bits.toLowerCase();
  }

  this.camel = utils.stringCase(bits, 'camel');
  this.capitalCamel = utils.stringCase(bits, 'capitalCamel');
  this.kebab = utils.stringCase(bits, 'kebab');
  this.snake = utils.stringCase(bits, 'snake');
  this.flat = utils.stringCase(bits, 'flat');
  this.dot = utils.stringCase(bits, 'dot');
}

StringCase.prototype.toString = function () {
  return this.asIs;
};