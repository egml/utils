"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Name;

var utils = _interopRequireWildcard(require("./utils"));

var _StringCase = _interopRequireDefault(require("./StringCase"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function Name(nameBits, nsBits) {
  var bits; // Далее идет обработка параметра nsBits:
  // - если он не задан, пропустить все и взять ns из прототипа
  // - если он boolean и true, то также пропустить и использовать ns 
  //   из прототипа
  // - если он boolean и false, то nsBits - пустая строка
  // - и если в результате nsBits строка или массив, то создать новое 
  //   свойство ns поверх геттера прототипа со значением nsBits

  if (nsBits != null) {
    if (typeof nsBits == 'boolean' && nsBits == false) {
      nsBits = '';
    }

    if (typeof nsBits == 'string' || Array.isArray(nsBits)) {
      Object.defineProperty(this, 'ns', {
        value: new _StringCase.default(nsBits),
        writable: true,
        configurable: true,
        enumerable: true
      });
    }
  }

  var hasNs = this.ns && this.ns.toString() != '';
  this.base = new _StringCase.default(nameBits); // #asIs #flat #camel #capital #kebab

  var types = ['asIs', 'flat', 'camel', 'capitalCamel', 'kebab'];

  for (var i = 0; i < types.length; i++) {
    if (hasNs) {
      this['_' + types[i]] = utils.stringCase([this.ns[types[i]], this.base[types[i]]], types[i]);
    } else {
      this['_' + types[i]] = this.base[types[i]];
    }

    this[types[i]] = function (type, nameObj) {
      return function (suffix) {
        if (suffix) {
          if (type != 'asIs') {
            if (Array.isArray(suffix)) {
              suffix.forEach(function (bit, i) {
                suffix[i] = bit.toLowerCase();
              });
            }
          }

          return utils.stringCase([nameObj['_' + type], utils.stringCase(suffix, type)], type);
        } else {
          return nameObj['_' + type];
        }
      };
    }(types[i], this);
  } //  ██████   ██████  ████████
  //  ██   ██ ██    ██    ██
  //  ██   ██ ██    ██    ██
  //  ██   ██ ██    ██    ██
  //  ██████   ██████     ██
  //
  // #dot


  var bits = [this.base.camel];

  if (hasNs) {
    bits.unshift(this.ns.camel);
  }

  this._dot = utils.stringCase(bits, 'dot');

  this.dot = function (suffix) {
    if (suffix) {
      if (Array.isArray(suffix)) {
        suffix.forEach(function (bit, i) {
          suffix[i] = bit.toLowerCase();
        });
      }

      return utils.stringCase([this._dot, utils.stringCase(suffix, 'camel')], 'dot');
    } else {
      return this._dot;
    }
  }; //  ███████ ██    ██ ███████ ███    ██ ████████
  //  ██      ██    ██ ██      ████   ██    ██
  //  █████   ██    ██ █████   ██ ██  ██    ██
  //  ██       ██  ██  ██      ██  ██ ██    ██
  //  ███████   ████   ███████ ██   ████    ██
  //
  // #event


  this.event = this.dot; //   ██████ ███████ ███████
  //  ██      ██      ██
  //  ██      ███████ ███████
  //  ██           ██      ██
  //   ██████ ███████ ███████
  //
  // #css

  bits = [this.base.snake];

  if (hasNs) {
    bits.unshift(this.ns.snake);
  }

  this._css = utils.stringCase(bits, 'kebab');

  this.css = function (suffix) {
    if (suffix) {
      if (Array.isArray(suffix)) {
        suffix.forEach(function (bit, i) {
          suffix[i] = bit.toLowerCase();
        });
      }

      return utils.stringCase([this._css, utils.stringCase(suffix, 'snake')], 'kebab');
    } else {
      return this._css;
    }
  }; //   ██████ ███████ ███████
  //  ██      ██      ██
  //  ██      ███████ ███████
  //  ██           ██      ██
  //   ██████ ███████ ███████
  // 
  //  ███    ███  ██████  ██████
  //  ████  ████ ██    ██ ██   ██
  //  ██ ████ ██ ██    ██ ██   ██
  //  ██  ██  ██ ██    ██ ██   ██
  //  ██      ██  ██████  ██████
  //
  // #css #modificator


  this.cssModificator = function (modificatorName) {
    if (modificatorName != null) {
      if (Array.isArray(modificatorName)) {
        modificatorName.forEach(function (bit, i) {
          modificatorName[i] = bit.toLowerCase();
        });
      }

      return this.css() + '--' + utils.stringCase(modificatorName, 'snake');
    } else {
      return this.css();
    }
  };

  this.cssMod = this.cssModificator; //   ██████ ███████ ███████
  //  ██      ██      ██
  //  ██      ███████ ███████
  //  ██           ██      ██
  //   ██████ ███████ ███████
  // 
  //   ██████ ██       █████  ███████ ███████
  //  ██      ██      ██   ██ ██      ██
  //  ██      ██      ███████ ███████ ███████
  //  ██      ██      ██   ██      ██      ██
  //   ██████ ███████ ██   ██ ███████ ███████
  // 
  //  ███████ ███████ ██      ███████  ██████ ████████  ██████  ██████
  //  ██      ██      ██      ██      ██         ██    ██    ██ ██   ██
  //  ███████ █████   ██      █████   ██         ██    ██    ██ ██████
  //       ██ ██      ██      ██      ██         ██    ██    ██ ██   ██
  //  ███████ ███████ ███████ ███████  ██████    ██     ██████  ██   ██
  //
  // #css #class #selector

  this.cssClassSelector = function (appendage) {
    return '.' + this.css(appendage);
  };

  this.selector = function (appendage) {
    return this.cssClassSelector(appendage);
  };
}

var ns;
Object.defineProperty(Name.prototype, 'ns', {
  set: function (value) {
    ns = new _StringCase.default(value);
  },
  get: function () {
    return ns;
  }
});

Name.prototype.toString = function () {
  return this._asIs;
};