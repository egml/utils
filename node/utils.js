"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.request = request;
exports.importSvg = importSvg;
exports.importSVG = importSVG;
exports.extend = extend;
exports.childByClass = childByClass;
exports.pageScroll = pageScroll;
exports.closestParentByClass = closestParentByClass;
exports.isObject = isObject;
exports.rem = rem;
exports.contentBoxHeight = contentBoxHeight;
exports.marginBoxHeight = marginBoxHeight;
exports.createWidgets = createWidgets;
exports.stringCase = stringCase;
exports.firstUpperCase = firstUpperCase;
exports.firstLowerCase = firstLowerCase;
exports.injectStyle = injectStyle;
exports.injectScript = injectScript;
exports.injectHtml = injectHtml;
exports.injectSvgSymbol = injectSvgSymbol;

//  ██████  ███████  ██████  ██    ██ ███████ ███████ ████████
//  ██   ██ ██      ██    ██ ██    ██ ██      ██         ██
//  ██████  █████   ██    ██ ██    ██ █████   ███████    ██
//  ██   ██ ██      ██ ▄▄ ██ ██    ██ ██           ██    ██
//  ██   ██ ███████  ██████   ██████  ███████ ███████    ██
//                      ▀▀
// #request
function request(options) {
  var xhr = new XMLHttpRequest();
  options.type = options.type || 'get';
  xhr.open(options.type, options.url); // Yii проверяет наличие хедера X-Requested-With в фильтре ajaxOnly
  // http://www.yiiframework.com/doc/api/1.1/CHttpRequest#getIsAjaxRequest-detail

  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
  xhr.onload = options.success;
  xhr.send(options.query);
}

; //  ██ ███    ███ ██████   ██████  ██████  ████████
//  ██ ████  ████ ██   ██ ██    ██ ██   ██    ██
//  ██ ██ ████ ██ ██████  ██    ██ ██████     ██
//  ██ ██  ██  ██ ██      ██    ██ ██   ██    ██
//  ██ ██      ██ ██       ██████  ██   ██    ██
// 
//  ███████ ██    ██  ██████
//  ██      ██    ██ ██
//  ███████ ██    ██ ██   ███
//       ██  ██  ██  ██    ██
//  ███████   ████    ██████
//
// #import #svg

function importSvg(url, loadEventName, target, idAttribute) {
  importSVG(url, loadEventName, target, idAttribute);
}

function importSVG(url, loadEventName, target, idAttribute) {
  loadEventName = loadEventName || 'svgload';
  target = target || document.body;
  if (typeof DOMParser !== 'undefined') // IE6-
    request({
      url: url,
      success: function () {
        // Для IE7-9, т.к. они не поддерживают встроенный парсинг свойства responseXML
        // http://msdn.microsoft.com/en-us/library/ie/ms535874%28v=vs.85%29.aspx
        var parser = new DOMParser();
        var xml = parser.parseFromString(this.responseText, 'text/xml'); // var xml = this.responseXML;

        var svg = document.importNode(xml.documentElement, true);

        if (idAttribute) {
          svg.setAttribute('id', idAttribute);
        }

        target.appendChild(svg);
        target.dispatchEvent(new CustomEvent(loadEventName, {
          bubbles: true
        }));
      }
    });
}

; //  ███████ ██   ██ ████████ ███████ ███    ██ ██████
//  ██       ██ ██     ██    ██      ████   ██ ██   ██
//  █████     ███      ██    █████   ██ ██  ██ ██   ██
//  ██       ██ ██     ██    ██      ██  ██ ██ ██   ██
//  ███████ ██   ██    ██    ███████ ██   ████ ██████
//
// #extend

function extend(dest, source) {
  for (var prop in source) {
    dest[prop] = source[prop];
  }

  return dest;
} //   ██████ ██   ██ ██ ██      ██████
//  ██      ██   ██ ██ ██      ██   ██
//  ██      ███████ ██ ██      ██   ██
//  ██      ██   ██ ██ ██      ██   ██
//   ██████ ██   ██ ██ ███████ ██████
// 
//  ██████  ██    ██
//  ██   ██  ██  ██
//  ██████    ████
//  ██   ██    ██
//  ██████     ██
// 
//   ██████ ██       █████  ███████ ███████
//  ██      ██      ██   ██ ██      ██
//  ██      ██      ███████ ███████ ███████
//  ██      ██      ██   ██      ██      ██
//   ██████ ███████ ██   ██ ███████ ███████
//
// #child #class


function childByClass(parent, className) {
  var children = parent.children;

  for (var i = 0; i < children.length; i++) {
    if (children[i].classList.contains(className)) {
      return children[i];
    }
  }
} //  ██████   █████   ██████  ███████
//  ██   ██ ██   ██ ██       ██
//  ██████  ███████ ██   ███ █████
//  ██      ██   ██ ██    ██ ██
//  ██      ██   ██  ██████  ███████
// 
//  ███████  ██████ ██████   ██████  ██      ██
//  ██      ██      ██   ██ ██    ██ ██      ██
//  ███████ ██      ██████  ██    ██ ██      ██
//       ██ ██      ██   ██ ██    ██ ██      ██
//  ███████  ██████ ██   ██  ██████  ███████ ███████
//
// #page #scroll
// Кросс-браузерное определение величины скролла


function pageScroll() {
  if (window.pageXOffset != undefined) {
    return {
      left: pageXOffset,
      top: pageYOffset
    };
  } else {
    var html = document.documentElement;
    var body = document.body;
    var top = html.scrollTop || body && body.scrollTop || 0;
    top -= html.clientTop;
    var left = html.scrollLeft || body && body.scrollLeft || 0;
    left -= html.clientLeft;
    return {
      left: left,
      top: top
    };
  }
} //   ██████ ██       ██████  ███████ ███████ ███████ ████████
//  ██      ██      ██    ██ ██      ██      ██         ██
//  ██      ██      ██    ██ ███████ █████   ███████    ██
//  ██      ██      ██    ██      ██ ██           ██    ██
//   ██████ ███████  ██████  ███████ ███████ ███████    ██
// 
//  ██████   █████  ██████  ███████ ███    ██ ████████
//  ██   ██ ██   ██ ██   ██ ██      ████   ██    ██
//  ██████  ███████ ██████  █████   ██ ██  ██    ██
//  ██      ██   ██ ██   ██ ██      ██  ██ ██    ██
//  ██      ██   ██ ██   ██ ███████ ██   ████    ██
// 
//  ██████  ██    ██
//  ██   ██  ██  ██
//  ██████    ████
//  ██   ██    ██
//  ██████     ██
// 
//   ██████ ██       █████  ███████ ███████
//  ██      ██      ██   ██ ██      ██
//  ██      ██      ███████ ███████ ███████
//  ██      ██      ██   ██      ██      ██
//   ██████ ███████ ██   ██ ███████ ███████
//
// #closest #parent #class


function closestParentByClass(element, className) {
  var parent = element.parentNode;

  while (!parent.classList.contains(className)) {
    parent = parent.parentNode;
  }

  return parent;
} //  ██ ███████
//  ██ ██
//  ██ ███████
//  ██      ██
//  ██ ███████
// 
//   ██████  ██████       ██ ███████  ██████ ████████
//  ██    ██ ██   ██      ██ ██      ██         ██
//  ██    ██ ██████       ██ █████   ██         ██
//  ██    ██ ██   ██ ██   ██ ██      ██         ██
//   ██████  ██████   █████  ███████  ██████    ██
//
// #is #object
// Сперто из interact.js (сперто немного, но все равно: http://interactjs.io/)


function isObject(thing) {
  return !!thing && typeof thing === 'object';
} //  ██████  ███████ ███    ███
//  ██   ██ ██      ████  ████
//  ██████  █████   ██ ████ ██
//  ██   ██ ██      ██  ██  ██
//  ██   ██ ███████ ██      ██
//
// #rem
// Подсчёт величины в rem. На входе пиксели без `px`, на выходе величина в rem с `rem`


var rootFontSize;

function rem(pxNoUnits, recalc) {
  if (typeof rootFontSize != 'number' || recalc) {
    // #CBFIX: Edge (42.17134.1.0, EdgeHTML 17.17134) и IE определяют значение как, например, 9.93 в результате >> операция дает 9, поэтому тут дополнительно надо округлять. UPD: Зачем переводить в integer с помощью >>, если мы уже применям к строке Math.round, тем самым она автоматически кастуется в integer.
    // rootFontSize = Math.round(window.getComputedStyle(document.documentElement).getPropertyValue('font-size').slice(0,-2)) >> 0;
    rootFontSize = Math.round(window.getComputedStyle(document.documentElement).getPropertyValue('font-size').slice(0, -2));
  }

  return pxNoUnits / rootFontSize + 'rem';
} //   ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████
//  ██      ██    ██ ████   ██    ██    ██      ████   ██    ██
//  ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██
//  ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██
//   ██████  ██████  ██   ████    ██    ███████ ██   ████    ██
// 
//  ██████   ██████  ██   ██
//  ██   ██ ██    ██  ██ ██
//  ██████  ██    ██   ███
//  ██   ██ ██    ██  ██ ██
//  ██████   ██████  ██   ██
// 
//  ██   ██ ███████ ██  ██████  ██   ██ ████████
//  ██   ██ ██      ██ ██       ██   ██    ██
//  ███████ █████   ██ ██   ███ ███████    ██
//  ██   ██ ██      ██ ██    ██ ██   ██    ██
//  ██   ██ ███████ ██  ██████  ██   ██    ██
//
// #content #box #height
// Подсчёт высоты content-box с дробью (в отличие от $(...).height())


function contentBoxHeight(element) {
  var style = window.getComputedStyle(element);
  return element.getBoundingClientRect().height - style.getPropertyValue('padding-top').slice(0, -2) - style.getPropertyValue('padding-bottom').slice(0, -2) - style.getPropertyValue('border-top-width').slice(0, -2) - style.getPropertyValue('border-bottom-width').slice(0, -2);
} //  ███    ███  █████  ██████   ██████  ██ ███    ██
//  ████  ████ ██   ██ ██   ██ ██       ██ ████   ██
//  ██ ████ ██ ███████ ██████  ██   ███ ██ ██ ██  ██
//  ██  ██  ██ ██   ██ ██   ██ ██    ██ ██ ██  ██ ██
//  ██      ██ ██   ██ ██   ██  ██████  ██ ██   ████
// 
//  ██████   ██████  ██   ██
//  ██   ██ ██    ██  ██ ██
//  ██████  ██    ██   ███
//  ██   ██ ██    ██  ██ ██
//  ██████   ██████  ██   ██
// 
//  ██   ██ ███████ ██  ██████  ██   ██ ████████
//  ██   ██ ██      ██ ██       ██   ██    ██
//  ███████ █████   ██ ██   ███ ███████    ██
//  ██   ██ ██      ██ ██    ██ ██   ██    ██
//  ██   ██ ███████ ██  ██████  ██   ██    ██
//
// #margin #box #height


function marginBoxHeight(element, sourceWindow) {
  sourceWindow = sourceWindow || window;
  var style;

  if (element && (style = sourceWindow.getComputedStyle(element))) {
    return element.getBoundingClientRect().height + (style.getPropertyValue('margin-top').slice(0, -2) >> 0) + (style.getPropertyValue('margin-bottom').slice(0, -2) >> 0);
  } else {
    return 0;
  }
} //   ██████ ██████  ███████  █████  ████████ ███████
//  ██      ██   ██ ██      ██   ██    ██    ██
//  ██      ██████  █████   ███████    ██    █████
//  ██      ██   ██ ██      ██   ██    ██    ██
//   ██████ ██   ██ ███████ ██   ██    ██    ███████
// 
//  ██     ██ ██ ██████   ██████  ███████ ████████ ███████
//  ██     ██ ██ ██   ██ ██       ██         ██    ██
//  ██  █  ██ ██ ██   ██ ██   ███ █████      ██    ███████
//  ██ ███ ██ ██ ██   ██ ██    ██ ██         ██         ██
//   ███ ███  ██ ██████   ██████  ███████    ██    ███████
//
// #create #widgets

/**
 * Создает объекты виджета в определенном контексте.
 * 
 * @param {function|class} constructor Конструктор виджета
 * @param {DOMElement} context В контексте (внутри) какого элемента производить поиск 
 * элементов для инициализации виджета. Если не указан, используется document.
 * @param {object} options Конфигурационный объект, передаваемый создаваемому виджету
 * @param {string} selector Какой селектор использовать для поиска элементов. 
 * Если не указать, используется name.css из прототипа виджета.
 */


function createWidgets(constructor, context, options, selector) {
  if (typeof constructor == 'undefined') {
    throw new Error('Нужно обязательно указать класс виджета');
  }

  context = context || document;
  selector = selector || '.' + constructor.prototype.name.css;
  var elements = context.querySelectorAll(selector);

  if (elements.length) {
    for (var i = 0; i < elements.length; i++) {
      // Почему-то try...catch выдает куда меньше полезной информации, чем простая обработка ошибок в консоли браузера
      // try {
      new constructor(elements[i], options); // } catch(error) {
      // 	console.error(error);
      // }
    }
  } else {
    throw new Error('Элементы в DOM в выбранном контексте по такому селектору не найдены');
  }
} //  ███████ ████████ ██████  ██ ███    ██  ██████
//  ██         ██    ██   ██ ██ ████   ██ ██
//  ███████    ██    ██████  ██ ██ ██  ██ ██   ███
//       ██    ██    ██   ██ ██ ██  ██ ██ ██    ██
//  ███████    ██    ██   ██ ██ ██   ████  ██████
// 
//   ██████  █████  ███████ ███████
//  ██      ██   ██ ██      ██
//  ██      ███████ ███████ █████
//  ██      ██   ██      ██ ██
//   ██████ ██   ██ ███████ ███████
//
// #string #case


function stringCase(bits, type) {
  if (!bits || !bits.length) {
    return '';
  }

  if (typeof bits == 'string') {
    bits = [bits];
  }

  var string = '';

  switch (type) {
    case 'camel':
      bits.forEach(function (bit, i) {
        if (i == 0) {
          string += firstLowerCase(bit);
        } else {
          string += firstUpperCase(bit);
        }
      });
      break;

    case 'capitalCamel':
      bits.forEach(function (bit) {
        string += firstUpperCase(bit);
      });
      break;

    case 'flat':
      string = bits.join('');
      break;

    case 'snake':
      string = bits.join('_');
      break;

    case 'kebab':
      string = bits.join('-');
      break;

    case 'dot':
      string = bits.join('.');
      break;

    case 'asIs':
    default:
      string = bits.join('');
      break;
  }

  return string;
} //  ███████ ██ ██████  ███████ ████████
//  ██      ██ ██   ██ ██         ██
//  █████   ██ ██████  ███████    ██
//  ██      ██ ██   ██      ██    ██
//  ██      ██ ██   ██ ███████    ██
// 
//  ██    ██ ██████  ██████  ███████ ██████      ██
//  ██    ██ ██   ██ ██   ██ ██      ██   ██    ██
//  ██    ██ ██████  ██████  █████   ██████    ██
//  ██    ██ ██      ██      ██      ██   ██  ██
//   ██████  ██      ██      ███████ ██   ██ ██
// 
//  ██       ██████  ██     ██ ███████ ██████
//  ██      ██    ██ ██     ██ ██      ██   ██
//  ██      ██    ██ ██  █  ██ █████   ██████
//  ██      ██    ██ ██ ███ ██ ██      ██   ██
//  ███████  ██████   ███ ███  ███████ ██   ██
// 
//   ██████  █████  ███████ ███████
//  ██      ██   ██ ██      ██
//  ██      ███████ ███████ █████
//  ██      ██   ██      ██ ██
//   ██████ ██   ██ ███████ ███████
//
// #first #upper #lower #case


function firstUpperCase(string) {
  return string[0].toUpperCase() + string.slice(1);
}

;

function firstLowerCase(string) {
  return string[0].toLowerCase() + string.slice(1);
}

; // #TODO: Написать функционал логинга переменной или объекта в HTML
// //  ██████   ██████  ███    ███
// //  ██   ██ ██    ██ ████  ████
// //  ██   ██ ██    ██ ██ ████ ██
// //  ██   ██ ██    ██ ██  ██  ██
// //  ██████   ██████  ██      ██
// // 
// //  ██       ██████   ██████
// //  ██      ██    ██ ██
// //  ██      ██    ██ ██   ███
// //  ██      ██    ██ ██    ██
// //  ███████  ██████   ██████
// //
// // #dom #log
// export function domLog(object, target) {
// 	if (typeof target == 'undefined') {
// 		target = document;
// 	}
// 	var output = '';
// 	if (typeof object == 'object') {
// 		for (var key in object) {
// 			if (object.hasOwnProperty(key)) {
// 				var element = object[key];
// 				output += '';
// 			}
// 		}
// 	}
// 	'<div style="font-family:monospace; font-size:1rem; white-space:nowrap;">' + 
// 	'width &nbsp;= ' + rect.width + '<br>' +
// 	'height = ' + rect.height + '<br>' +
// 	'x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= ' + rect.x + '<br>' +
// 	'y &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= ' + rect.y + '<br>' +
// 	'left &nbsp;&nbsp;= ' + rect.left + '<br>' +
// 	'top &nbsp;&nbsp;&nbsp;= ' + rect.top + '<br>' +
// 	'right &nbsp;= ' + rect.right + '<br>' +
// 	'bottom = ' + rect.bottom + '<br>' +
// 	'</div>';
// }
//	██ ███    ██      ██ ███████  ██████ ████████
//	██ ████   ██      ██ ██      ██         ██
//	██ ██ ██  ██      ██ █████   ██         ██
//	██ ██  ██ ██ ██   ██ ██      ██         ██
//	██ ██   ████  █████  ███████  ██████    ██
// 
//	███████ ████████ ██    ██ ██      ███████
//	██         ██     ██  ██  ██      ██
//	███████    ██      ████   ██      █████
//	     ██    ██       ██    ██      ██
//	███████    ██       ██    ███████ ███████
// 
// #inject #style

function injectStyle() {
  var options = {
    url: null,
    content: null,
    inline: null,
    targetDocument: null,
    onLoadCallback: null
  };
  var mountElement = null;
  options.inline = false;
  options.targetDocument = document;
  Object.defineProperty(options, 'mountElement', {
    get: function () {
      if (mountElement == null) {
        return options.targetDocument.head;
      } else {
        return mountElement;
      }
    },
    set: function (value) {
      mountElement = value;
    },
    configurable: true,
    enumerable: true
  });

  if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
    options.url = arguments[0];
    options.onLoadCallback = arguments[1];
  } else if (!!arguments[0] && typeof arguments[0] == 'object') {
    extend(options, arguments[0]);
  } else {
    return;
  }

  if (options.url) {
    if (options.inline == false) {
      var tag = options.targetDocument.createElement('link');
      tag.rel = 'stylesheet';

      if (typeof options.onLoadCallback == 'function') {
        tag.addEventListener('load', options.onLoadCallback);
      }

      tag.href = options.url;
      options.targetDocument.head.appendChild(tag);
    } else {
      var xhr = new XMLHttpRequest();
      xhr.open('get', options.url);

      xhr.onload = function () {
        var tag = options.targetDocument.createElement('style');
        tag.textContent = this.responseText;
        options.mountElement.appendChild(tag);

        if (typeof options.onLoadCallback == 'function') {
          options.onLoadCallback();
        }
      };

      xhr.send();
    }
  } else if (options.content) {
    var tag = options.targetDocument.createElement('style');
    tag.textContent = options.content;
    options.mountElement.appendChild(tag);

    if (typeof options.onLoadCallback == 'function') {
      options.onLoadCallback();
    }
  } else {
    return;
  }
} //	██ ███    ██      ██ ███████  ██████ ████████
//	██ ████   ██      ██ ██      ██         ██
//	██ ██ ██  ██      ██ █████   ██         ██
//	██ ██  ██ ██ ██   ██ ██      ██         ██
//	██ ██   ████  █████  ███████  ██████    ██
// 
//	███████  ██████ ██████  ██ ██████  ████████
//	██      ██      ██   ██ ██ ██   ██    ██
//	███████ ██      ██████  ██ ██████     ██
//	     ██ ██      ██   ██ ██ ██         ██
//	███████  ██████ ██   ██ ██ ██         ██
// 
//	#inject #script


function injectScript() {
  var options = {
    url: null,
    content: null,
    inline: null,
    targetDocument: null,
    onLoadCallback: null
  };
  var mountElement = null;
  options.inline = false;
  options.targetDocument = document;
  Object.defineProperty(options, 'mountElement', {
    get: function () {
      if (mountElement == null) {
        return options.targetDocument.head;
      } else {
        return mountElement;
      }
    },
    set: function (value) {
      mountElement = value;
    },
    configurable: true,
    enumerable: true
  });

  if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
    options.url = arguments[0];
    options.onLoadCallback = arguments[1];
  } else if (!!arguments[0] && typeof arguments[0] == 'object') {
    extend(options, arguments[0]);
  } else {
    return;
  }

  var tag = options.targetDocument.createElement('script');

  if (options.url) {
    if (options.inline == false) {
      if (typeof options.onLoadCallback == 'function') {
        tag.addEventListener('load', options.onLoadCallback);
      }

      tag.src = options.url;
      options.mountElement.appendChild(tag);
    } else {
      var xhr = new XMLHttpRequest();
      xhr.open('get', options.url);

      xhr.onload = function () {
        tag.textContent = this.responseText;
        options.mountElement.appendChild(tag);

        if (typeof options.onLoadCallback == 'function') {
          options.onLoadCallback();
        }
      };

      xhr.send();
    }
  } else if (options.content) {
    tag.textContent = options.content;
    options.mountElement.appendChild(tag);

    if (typeof options.onLoadCallback == 'function') {
      options.onLoadCallback();
    }
  } else {
    return;
  }
} //	██ ███    ██      ██ ███████  ██████ ████████
//	██ ████   ██      ██ ██      ██         ██
//	██ ██ ██  ██      ██ █████   ██         ██
//	██ ██  ██ ██ ██   ██ ██      ██         ██
//	██ ██   ████  █████  ███████  ██████    ██
// 
//	██   ██ ████████ ███    ███ ██
//	██   ██    ██    ████  ████ ██
//	███████    ██    ██ ████ ██ ██
//	██   ██    ██    ██  ██  ██ ██
//	██   ██    ██    ██      ██ ███████
// 
//	#inject


function injectHtml() {
  var options = {
    html: null,
    legacy: false,
    url: null,
    targetDocument: document,
    onLoadCallback: null
  };
  var mountElement = null;
  Object.defineProperty(options, 'mountElement', {
    get: function () {
      if (mountElement == null) {
        return options.targetDocument.body;
      } else {
        return mountElement;
      }
    },
    set: function (value) {
      mountElement = value;
    },
    configurable: true,
    enumerable: true
  });

  if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
    options.html = arguments[0];
  } else if (!!arguments[0] && typeof arguments[0] == 'object') {
    extend(options, arguments[0]);
  } else {
    return;
  }

  if (options.url) {
    var xhr = new XMLHttpRequest();
    xhr.open('get', options.url);

    xhr.onload = function () {
      if (options.legacy) {
        // #CBNOTE: Старые браузеры, такие как WebView 33, не поддерживают createContextualFragment (или что-то в этом духе), в общем, если ругаются, то вот так можно обойти:
        var parser = new DOMParser();
        var externalDom = parser.parseFromString('<div id="__imported_html__">' + this.responseText + '</div>', 'text/html'); // #TODO: В данном случае вставится <div id="__imported_html__">...полученный HTML...</div>, это надо исправить на некую возможно последовательную вставку всех дочерних нодов этого div'a

        var domFragment = options.targetDocument.importNode(externalDom.getElementById('__imported_html__'), true);
      } else {
        var domFragment = options.targetDocument.createRange().createContextualFragment(this.responseText);
      }

      options.mountElement.appendChild(domFragment);

      if (typeof options.onLoadCallback == 'function') {
        options.onLoadCallback();
      }
    };

    xhr.send();
  } else if (options.html) {
    if (options.legacy) {
      // #CBNOTE: Старые браузеры, такие как WebView 33, не поддерживают createContextualFragment (или что-то в этом духе), в общем, если ругаются, то вот так можно обойти:
      var parser = new DOMParser();
      var externalDom = parser.parseFromString('<div id="__imported_html__">' + options.html + '</div>', 'text/html');
      var domFragment = options.targetDocument.importNode(externalDom.getElementById('__imported_html__'), true);
    } else {
      var domFragment = options.targetDocument.createRange().createContextualFragment(options.html);
    }

    options.mountElement.appendChild(domFragment);

    if (typeof options.onLoadCallback == 'function') {
      options.onLoadCallback();
    }
  } else {
    return;
  }
} //	██ ███    ██      ██ ███████  ██████ ████████
//	██ ████   ██      ██ ██      ██         ██
//	██ ██ ██  ██      ██ █████   ██         ██
//	██ ██  ██ ██ ██   ██ ██      ██         ██
//	██ ██   ████  █████  ███████  ██████    ██
// 
//	███████ ██    ██  ██████
//	██      ██    ██ ██
//	███████ ██    ██ ██   ███
//	     ██  ██  ██  ██    ██
//	███████   ████    ██████
// 
//	███████ ██    ██ ███    ███ ██████   ██████  ██
//	██       ██  ██  ████  ████ ██   ██ ██    ██ ██
//	███████   ████   ██ ████ ██ ██████  ██    ██ ██
//	     ██    ██    ██  ██  ██ ██   ██ ██    ██ ██
//	███████    ██    ██      ██ ██████   ██████  ███████
// 
//	#inject #svg #symbol


function injectSvgSymbol() {
  // -------------------------------------------
  //  Вариант с отдельным родительским
  // 	SVG-элементом для каждого символа:
  // -------------------------------------------
  var options = {
    symbol: null,
    mountElement: document.body,
    containerClass: null
  };

  if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
    options.symbol = arguments[0];
    options.mountElement = arguments[1] || options.mountElement;
    options.containerClass = arguments[2] || options.containerClass;
  } else if (!!arguments[0] && typeof arguments[0] == 'object') {
    extend(options, arguments[0]);
  } else {
    return;
  }

  var parser = new DOMParser();
  var symbolDocument = parser.parseFromString(`<svg xmlns="http://www.w3.org/2000/svg" 
			${options.containerClass ? `class="${options.containerClass}"` : ''} 
			style="display: none;">
			${options.symbol}
		</svg>`, 'image/svg+xml');
  var symbolNode = document.importNode(symbolDocument.documentElement, true);
  options.mountElement.appendChild(symbolNode); // -------------------------------------------
  //  Вариант со вставкой в один родительский 
  // 	SVG-элемент:
  // -------------------------------------------
  // var options = {
  // 	symbol: null,
  // 	containerId: 'svg_sprite',
  // 	mountElement: document.body,
  // };
  // if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
  // 	options.symbol = arguments[0];
  // 	options.containerId = arguments[1] || options.containerId;
  // 	options.mountElement = arguments[2] || options.mountElement;
  // } else if (!!arguments[0] && typeof arguments[0] == 'object') {
  // 	extend(options, arguments[0]);
  // } else {
  // 	return;
  // }
  // var container = document.getElementById(options.containerId);
  // if (container == null) {
  // 	container = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  // 	container.setAttribute('id', options.containerId);
  // 	container.style.cssText = 'display: none;';
  // 	options.mountElement.appendChild(container);
  // }
  // var parser = new DOMParser();
  // var symbolDocument = parser.parseFromString(
  // 	`<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
  // 		${options.symbol}
  // 	</svg>`,
  // 	'image/svg+xml'
  // );
  // var symbolNode = document.importNode(symbolDocument.querySelector('symbol'), true);
  // container.appendChild(symbolNode);
}